---
slug: "ruby"
title: "RUBY"
---
Types
-----

### Objects

```
:any_object_has.object_id # Fixnum id of object
:any_object_has.class # => Symbol
:any_object_has.class.superclass # => Object

(123.instance_of? Fixnum) == (123.is_a? Fixnum) # => True 
(123.instance_of? Integer) == (123.is_a? Integer ) # => False; is_a? goes deeper for super classes 
Integer===123 # => True; works like is_a?
```

#### comparing objects

```
a = 'Ruby'
b = 'Ruby'
a.equal? b # => False, because a.object_id != b.object_id
a == b   # => True, Mosr classes redefine '==' operator

1 == 1.0 # => True, conversion to one type
1.eql? 1.0 # => False, because 1.hash != 1.0.hash
```

#### ordering objects

```
# <=> operator defines ordering in classes
include Comparable
def <=>(another_sock)
  self.size <=> another_sock.size
end
# so you can compare Socks with ==, !=, <, >, <=, >=, between?
sock1 >= sock2
sock1.between?(s2,s3)

# some Classes defines == operator explicitil if it can be more efficient that using <=>
```

#### converting

```
[1.to_s, 1.to_f, 1.to_s.to_f.to_i] # => ["1", 1.0, 1]

1.to_a # NoMethodError
Array.try_convert 1 # => nil 
Array 1 # => [1]; Kernel methods
```

#### copying

```
a = %W[a b c]
b = a.dup # or a.clone
b.equal?(a) # => False
b[0].equal?(a[0])] # => True
```

#### serializing

```
serialized = Marshal.dump myObject

def deepcopy(o)
  Marshal.load(Marshal.dump(o))
end
```

#### freezing

```
ice = 'ice'
ice.freeze
ice.upcase! # TypeError; after freezing object cant be modified
```

### Numeric

```
Numeric: Integer, Float, Complex, BigDecimal, Rational
Integer: Fixnum, Bignum

0377 # 255 with base 8
0b1111_1111 # 255 with base 2
0xFF # 255 with base 16

6.02e23 # 6.02 x 10**23
1_000_000.01
```

#### arithmetic

```
[5/2, 5.0/2, 5/2.0] # => [2,2.5,2.5]
[5.0.div(2), 5.0.fdiv(2), 5.quo(2)] => [2, 2.5, Rational(5,2)]

10%3 # => 1
q,r = 10.divmod 3 # => [3,1]

[-7/3, -7%3] # => [-3,2]; ruby rounds to minus infinity

x**4 # x*x*x*x
x**-1 # 1/x
x**(1/3.0) # 3rd level sqrt

4[2] # => 1; indexed binary 4
```

### Strings

```
"Next number by #{my_num} is #{my_num+1}"
"Hello #$global_var "

sprintf("Value of PI is about %.4f", Math::PI)
"Value of PI is about %.4f" % Math::PI

1000.times{ puts 'text'.object_id } # 1000 different objects are created, use variable instead
```

#### single quoted string

```
'single quoted string'
%q[i'm a single quoted string]

document = << 'HEREDOC'  # comment for document may be placed here
Here is a bunch of text
without any special characters
HEREDOC 
```

#### double quoted string

```
"double\tquoted\tstring"
%Q-i'm a "double quoted string"-
%{so Am i}

document = << HEREDOC # comment for document may be placed here
Here is a bunch of text
with special characters
HEREDOC 
```

#### single characters

```
?a == "a" # => true
?\123 # => "S"
?\t # => "\t"
?\C-x # Ctrl-X
```

#### operators

```
'hello' + 3 # TypeError
'hello' + 3.to_s == 'hello' << 3.to_s # => true
'hello' + 3.to_s == "hello%d" % 3 # => true

h = 'Hello'
h << ' world' # => "Hello world"
h == 'hello' # => false
h << 46 # "Hello world."

ellipsis = '.' * 3 # => "..."
```

#### "\[\]" operator

```
'something'[3] == 'something'[3,1] # => true
'something'[3] == 'something'[3,1] # => false; in Ruby 1.8 index 3 returns 101 instead 'm' 

word = 'windy'
word[-1] = 'surfing' # => "surfing"; word is "windsurfing"
word[0,4] = 'train' # => "train"; word is "trainsurfing"

word[word.length-1,100] # => "g"
word[word.length,100] # => ""
word[word.length+1,100] # => nil
word + word[-3..-2] + word [1..4] # => "trainsurfinginrain"

word['surf'] # => "surf"
word['smurf'] # => nil
word['surf'] = 'smurf' # => "smurf"; word is "trainsmurfing"
```

#### string iterators

```
's'.each_line # 's'.each in 1.8
's'.each_char # missing in 1.8
's'.each_byte
```

### Arrays

```
Array.new # => []
Array.new(2) # => [nil,nil]
stars = Array.new(2,'*') # => ['*','*']
Array.new(stars) # => ['*','*']; copy of stars
Array.new(3) {|i| '*' * (i+1)} # => ["*", "**", "***"] 
```

#### Arrays of strings

```
names = %w[bonnie clyde] # array of single quoted strings
specials = %W[\n \t \s] # array of double quoted strings
```

#### "\[\]" operator

```
# see "[]" operator in strings
```

#### operations

```
a = [1,2,3] + [2,1,0] # => [1,2,3,2,1,0]
a -= [1,3] # =>  [2,2,0]
a *= 2 # => [2,2,0,2,2,0]

a & [-2,2] # => [2]; unique elements from intersection 
a | [-2,2] # => [2,0,-2]; unique elements from sum
```

### Hashes

```
# symbols are more efficient as hash keys than strings

Hash.new # => {}
Hash.new[:key] # => nil
Hash.new('zzz')[:key] # => "zzz"

{:a => 'first'} == {a: 'first'} # => true; not available in 1.8
```

### Ranges

```
int_range = 1..10
int_range.include? 10 # => True
(-0.99...0.99).include? 0.99 # => False
```

#### discrete ranges

```
# class of range limits must define 'succ' method
(1..10).step(2){} # Enumerable methods may be used
('a'..'c').each {} # discrete because 'a'.succ == 'b'
```

### Symbols

```
:symbol
%s[:] # => :":"
s = 'symbol'; :"#{s}" # => :symbol 

name = :size
if o.respond_to? name
  o.send(name)
end
```

#### to/from String

```
:symbol.to_s == :symbol.id2name
'string'.to_sym == 'string'.intern
```

Expressions
-----------

### assignments

```
x,y = y,x # parallel, variables switch values
x = y; y = x # sequence, variables have values of y

x = 1,2,3 # x is [1,2,3]
x, = 1,2,3 # x is 1
```

#### splat operator

```
x, y, z = 1, [2,3] # y is [2,3], z is nil
x, y, z = 1, *[2,3] # equals to x, y, z = 1, 2, 3

x, *y = 1, 2, 3 # x is 1, y is [2,3]
x, *y = 1 # x is 1, y is []

x, y, *z = 1, *[2,3,4] # x is 1, y is 2, z is [3,4]
```

### flip flop

```
(1..10).each {|x| puts x if x==3..x>=3 } # prints 3; in one step gets left true and right false
(1..10).each {|x| puts x if x==3...x>=3 } # prints 3, 4; checks right false one step after getting left true

ARGF.each do |line|
  print line if line=~/TODO/..line=~/^$/ # print from first TODO to empty row
end
```

### defined?

```
x = f(x) if defined? f(x) # defined? does not calculate f(x), only checks if f is defined and x is defined

defined? local_variable # => 'local-variable'; defined? returns strings
defined? nil # => 'nil'; string
```

Statements & flow control
-------------------------

### conditional statements

#### if, unless, ?:

```
if x == 1 # new line before code
  puts 'one'
elsif x == 2 then puts 'dwa' # or 'then'
elsif x == 3; puts 'three' # or semicolon
end # end is required

y = x.invert if x.respond_to? :invert # nothing happens if there is no x.invert
y = (x.invert if x.respond_to? :invert) # y=nil if there is no x.invert
```

#### case

```
case # simple case, nothing after case
  when x==1 
    puts 'one'
  when x==2 then puts 'two'
  when x==3; puts 'three'
  else puts 'more' # else is optional
end

case x # expression after case makes that '===' operator is used
  when String then 'it is string' # Class=== checks if expression is an object of Class
  when 0..9 then 'it is digit' # range=== checks if expression belongs to range
  when /awesome/ then puts 'there is something awesome' # regexp=== matches expression
  when nil,[],'',0 then puts 'it is false' # equals to: nil===x || []===x || ''===x || 0===x
end
```

### loops

#### while, until

```
while x>0 do
  puts x-=1
end

puts x-=1 while x > 0

begin # condition checked after first occurence
  puts x-=1
end while x > 0
```

#### for in

```
for variable in collection do # collection is any object with 'each' method
  puts variable
end
```

### Iterators & enumerables

#### return, break, next, redo

#### times, upto, downto, step

```
3.times { puts 'knock' }
9.downto(0) { |x| puts "#{x}..." } # upto
0.step(0.5,0.025){|x| "Yet another glass of vodka, it is already #{x} l" }
```

#### map, select, reject, inject

```
squares = [1,2,3].map{|x| x*x} # => [1,4,9]; alias is 'collect'

evens = (1..9).select{|x| x % 2 == 0 } # => [2, 4, 6, 8] 
odds = (1..9).reject{|x| x % 2 == 0 } # => [1, 3, 5, 7, 9] 

max = (1..5).inject{|max,x| max>x ? max : x} # => 5
floatprod = (1..5).inject(1.0) {|p,x| p*x} # => 120.0; 1.0*1*2*3*4*5

def add *args
    args.inject :+
end
```

#### custom iterators

```
class Array
  def reverse_iterate
    current_index = self.size-1
    while current_index >= 0
      yield self[current_index]
      current_index -= 1
    end
  end
end

[1,3,5,7].reverse_iterate{|x| puts x}
```

### Blocks

#### block values

```
array.collect do |x|
  if x == nil
    0
  else
    [x,x*x]
  end
end

array.collect do |x|
  next 0 if x == nil # return some value from not last line
  next x, x*x # return two values 
end
```

#### variables scope

```
x = y = 0
1.upto(4) do |x;y| # x gets value from yield, y is nil until code in block change it
  y = x + 1
  puts y*y
end
[x,y] # => [0,0]
```

### Exceptions

#### raise

```
raise #=> RuntimeError: ; no message
raise 'omg sth failed' #=> RuntimeError: omg sth failed
raise RuntimeError, 'omg sth failed' #=> RuntimeError: omg sth failed 

raise TypeError, 'expected integer' if not n.is_a? Integer
if n < 1
  raise ArgumentError, "expected number greater than 0 but got #{n}", caller
end
```

#### rescue

```
# handle StandardError (or expcetion subclasses)
begin
  do_sth()
rescue => ex # ex optional
  puts "#{ex.class} #{ex.message}" # or $! or $ERROR_INFO (require 'English')
end

rescue Exception # all exceptions handled
rescue ArgumentError, TypeError => error # two exceptions classes handled in one way

# two exceptions handled in different way
begin 
  do_sth
rescue ArgumentError
  puts 'argument error'
rescueTypeError
  puts 'type error'
end

y = get_number() rescue 0 # one liner rescue 
```

#### retry

```
tries = 0
begin
  tries += 1
  try_to_fly()
rescue
  puts $!.message
  retry if tries < 5
end
```

#### else, ensure

```
# example using method
def method_name
  # method body
rescue
  # handling exceptions from method body
else
  # code executes if there were no exceptions nor return
ensure
  # code executes ALWAYS
end
```

### BEGIN, END, at\_exit

```
BEGIN { # executes always
  do_sth_at_start_of_program()
}

if (decision) # decide if you want to execute END code
  END {
    do_sth_at_the_end_of_program()
  }
end
```

Methods
-------

```
def sum(x,y); x+y; end
undef sum # method removed
```

#### singleton method

```
# does not work on Fixnum, Symbol and classes where objects are trated as values (in some ruby versions)
o = 'hello'
def o.louder # method defined only for o
  o << '!'
end
```

#### aliases

```
def hello
  puts 'Hello World'
end
alias original_hello hello
def hello # define new 'hello' method which uses old 'hello' method
  puts 'Attention please...'
  original_hello 
  puts 'Bye.'
end
```

### arguments

```
def prefix(s, len=1); s[0,len]; end # optional len has default value
def suffix(s, index=s.size-1); s[index,s.size-index]; end # default value as expression

def drink_sth(vodka,*available_extras) # available_extras is a table of optional arguments
what_we_got = %w(smirnoff ice_cubes coconut_liquor cola)
drink_sth(*what_we_got) # vodka => 'smirnoff', available_extras => ['ice_cubes','coconut_liquor','cola']

p = Proc.new{|x| x*x}
(1..9).map(&p)

%w(fred barney).map &:capitalize # => ['Fred','Barney']; uses Symbol.to_proc 
```

#### named

```
def rgb_colorize(args)
  r = args[:r] || 0
  b = args[:b] || 0
  g = args[:g] || 0
  ...
end

rgb_colorize({ :r => 123, :g => 98, :b => 201 })
rgb_colorize(:r => 123, :g => 98, :b => 201)
rgb_colorize r:123, g:98, b:201 # ruby 1.9
```

#### optional

```
def initialize(name, options={})
  @name = name
  @year = options[:year]
  # etc
end
```

#### optional block

```
class Timeline
  attr_accessor :tweets
  def print
    if block_given? # eg. timeline.print{|tweet| "tweet #{tweet}"}
      @tweets.each{|tweet| puts yield tweet}
    else # no block passed, eg. timeline.print
      puts tweets.join(', ')
    end
  end
end
```

### Proc

```
def makeproc(&p); p; end # converts block to Proc object & returns Proc object
adder = makeproc {|x,y| x+y} # adder is Proc.new{|x,y| x+y}
adder.call(2,3) # => 5

sum = Proc.new {|x,y| x+y} # proc type
sum = lambda {|x,y| x+y} # Kernel.lambda creates lambda type
sum = proc {|x,y| x+y} # Kernel.proc creates proc type, but in Ruby 1.8 lambda type 

sum == sum.dup # => true
```

#### "->" new lambda syntax

```
sum = ->(x,y){x+y}
zoom = ->x,y,factor=2{[x*factor,y*factor]} # default value of argument
f = ->x,y;a,b,c{...} # a,b,c are local variables, like in blocks
```

#### Proc as block

```
data.sort{|a,b| b-a} # sort requires block
data.sort &Proc.new{|a,b| b-a}
data.sort &->a,b{b-a} # 1.9
```

#### calling

```
product = Proc.new{|x,y| x*y}

product.call(x,y)
product[x,y] # []= is alias for call
product.(x,y) # Ruby 1.9

triple = product.curry.call(3) # Ruby 1.9; curry modifies Proc object
triple.call(7) # => 21
triple.call(7,1) # => 7
```

#### block as Proc

```
class Timeline
  attr_accessor :tweets
  def each(&block) # turns block into Proc, allows assign block to parameter
    # do anything with Proc, eg.
    @tweets.each(&block) # turn it to block again:) 
  end
end
```

#### Symbol.to\_proc

```
tweets.map(&:user) # same as tweets.map { |tweet| tweet.user }
```

#### arity

```
Proc.new{|x,y|}.arity # => 2 
Proc.new{|x,y,*z|}.arity # => -3; -n-1
~Proc.new{|x,y,*z|}.arity # => 2; ~(-n-1) == n
```

### Method class

```
m = 'something'.method(:capitalize) # => Method object representing capitalize of 'Something'
m.call # => 'Something'; or m[] ir m.()
[m.name, m.owner, m.receiver] #  => [:capitalize, String, "whatever"]; Ruby 1.9

def square(x); x*x; end
(1..10).map(&method(:square)) # converts method to proc to be used as block
```

Classes & Modules
-----------------

#### attributes

```
attr_reader :x, :y # getters
attr_accessor :x, :y # getters & setters

attr :x # only getter
attr :x, true # getter & setter
attr :x, :y # two getters, only Ruby 1.9
```

#### operators & duck typing

```
class Point
  ...
  def -@ 
    Point.new(-@x, -@y)
  end

  def +(other)
    Point.new(@x+other.x, @y+other.y)
  rescue # duck typing
    raise TypeError, "The point does not quack like a Point"  
  end

  def ==(other)
    # another duck typing, may be added checking is_a? Point 
    # or more resttrictive instance_of? Point
    @x==other.x && @y == other.y 
  rescue
    false # do not want to raise the error, false says that compared objects are not equal
  end  
end
```

#### Struct

```
Struct.new("Point",:x,:y) # => Struct::Point
Point = Struct.new(:x,:y) # => Point

p = Point.new(2,4); q = Point.new(2,4)
p == q # => True
p.eql? q # => True
p.to_s # => "#<struct Point x=2, y=4>" 

Point = Struct.new(:x,:y) do # struct extra methods
  def to_s
    "Points with coords: x{x} #{y}"
  end
end
```

#### alias\_method

```
class Point
  alias_method :as_string, :to_s
end
```

#### Class method

```
class Point
  def self.sum(*points) # or Point.sum(*points)
    x = y = 0
    points.each {|p| x+=p.x; y+=p.y}
  end
end
# or
class << Point # opening object of class Point for defining methods
  def sum(*points)
    ...
# or
class Point
  # define here object methods
  class << self
    # define here class methods
    def sum(*points)
      ...
```

#### Constants & Object/Class variables

```
class Point
  @@n = 0  #store number of points
  def initialize(x,y)
    @x, @y = x, y
    @@n+=1
  end
  ORIGIN = Point.new(0,0)
end

Point::ORIGIN => #<Point:...>
Point::UNIT = 1
```

### Methods visibility

```
# no arguments
class Point
  public # it is default & always is skipped
  # public methods
  protected # all methods below are protected until another visibility keyword
  #protected methods 
  private
  # private methods
end

# arguments
class Widget
  def x; ... end
  protected :x

  def self.sum; ... end;
  private_class_method :sum # symbol argument required for class methods visibility
end
```

#### access to private/protected

```
w = Widget.new

w.send :utility_method # call private method, Ruby 1.9.2 introduces public_send
w.instance_eval { utility_method } # another way

w.instance_eval {@x} # read the object variable
```

### Inheritance

```
AnyClass.is_a? Object # => True
Object.superclass # => BasicObject; since Ruby 1.9

class Point3d < Point
  def initialize(x,y,z)
    # @x and @y are not inherited, are not defined by classes
    # they exists since value assignment 
    super(x,y) 
    @z = z
  end
end
```

#### Struct

```
class Point3D < Struct.new('Point3D', :x, :y, :z) # defines '==', 'to_s', attr_accessors
  ... # add more interesting and custom methods
end
```

#### Abstract & Concrete

```
class AbstractGreeter # abstract class
  def greet
    puts "#{greeting} #{who}"
  end
end

class WorldGreeter < AbstractGreeter # concrete class
  def greeting; "Witaj"; end
  def who; "świecie"; end
end

WorldGreeter.new.greet # => Witaj świecie
```

### Constructors

#### new = allocate + initialize

```
def new(*args) # faked implementation to illustrate how 'new' works
  o = self.allocate    # method of Class class, cant be overloaded
  o.initialize(*args)  # overloaded in most cases
  o
end

Class#new    # object method to construct new objects
Class::new   # class method to define new classes
```

#### factory method as constructor

```
class Point
  def initialize(x,y)
    @x, @y = x, y  
  end
  private_class_method :new # hide default constructor
  def self.cartesian(x,y); 
    new(x,y)
  end
  def self.polar(r, theta)
    new(r*Math.cos(theta), r*Math.sin(theta))
  end
end
```

#### dup & clone

```
=flow
 1. allocate
 2. copy references(!) to object variables
 3. copy tainted 
 5. copy singleton methods (only clone)
 6. self.initialize_copy()
 7. freeze if origin is frozen (only clone)
=end
```

#### Singleton

```
require 'singleton'

class PointStats
  include Singleton # new & allocate are private, dup @ clone do not work, ...
  def initialize
    @n = 0
  end
  def record(point)
    @n += 1
  end
  def report; @n; end
end

PointStats.instance.record(myPoint)
```

### Modules

```
module XYZcode
  CODES = %w{x y z} # available via XYZcode::CODES
  def self.encode # or def XYZcode.encode
     ...
  end;
end
```

### Mixins

#### Module.include

```
module Alertable
  def whats_my_name? # object method instead of class method
    puts self.name
  end
end

class Animal
  attr_accessor :name
  include Alertable # my_animal.whats_my_name? works for all Animal objects
end

Animal.ancestors # => [Animal, Alertable, Object, Kernel, BasicObject]
Animal.included_modules # => [Alertable, Kernel]
```

#### Object.extend - single instance method

```
class Animal
  attr_accessor :name
end

module Alertable
  def whats_my_name? # object method instead of class method
    puts self.name
  end
end

(rex = Animal.new).name="Rex"
rex.extend(Alertable).whats_my_name? # => Rex; other Animal object does not have 'whats_my_name?'
```

#### Object.extend - class method

```
class Animal
  extend Welcomeable
  attr_accessor :name
end

module Welcomeable
  def welcome
    puts "Welcome my dear"
  end
end

Animal.welcome # extend makes it a class method, not instance method
```

#### included hook

```
module Utils
  def whats_my_name? # will be instance method
    puts self.name
  end
  module ClassMethods
    def welcome # will be class method
      puts "Welcome my dear"
    end
  end

  def self.included(base) # hook called when module is included in a class
    base.extend(ClassMethods)
  end
end
```

#### ActiveSupport::Concern

```
require 'active_support/concern'

module Utils
  extend ActiveSupport::Concern 

  def whats_my_name? # will be instance method
    puts self.name
  end

  module ClassMethods # AS::Concern looks for a module 'ClassMethods' to make all its methods as class methods 
    def welcome # will be class method
      puts "Welcome my dear"
    end
    def clean_up
      # make some extra work right after module is included
    end
  end

  included do 
    clean_up
  end

end
```

### Metaprogramming

#### define\_method

```
class Tweet
  [:draft, :posted, :deleted].each do |status| # creates 3 methods to change status, eg. my_tweet.posted 
    define_method status do 
      @status = status
    end
  end
end
```

#### class\_eval

```
Tweet.class_eval do # inside class_eval self is the Tweet class
  attr_accessor :user
end

class MethodLogger
  def log_method(klass,method_name)
    klass.class_eval do 
      alias_method "#{method_name}_original", method_name # save original method
      define_method method_name do |*args,&block| # take care of all args and block
        puts "#{Time.now} called #{method_name}"
        send "#{method_name}_original", *args, &block # call the original method
      end
    end
  end
end
```

#### instance\_eval

```
tweet = Tweet.new
tweet.instance_eval do # inside instance_eval self is the instance
  self.status = 'new status'
end

class Tweet
  def initialize(&block) # capture the block
    instance_eval(&block) if block_given? # run if given
  end
end
```

#### method\_missing

```
class Tweet
  attr_reader :user
  DELEGATED_METHODS = [:username, :avatar]

  def method_missing(method_name, *args)
    if DELEGATED_METHODS.include? method_name # delegate certain methods to @user, alternative is SimpleDelegator class
      @user.send(method_name, *args)
    else
      logger.warn "Tried call not existing #{method_name} with #{*args}" # write some log
      super # default method_missing raises NoMethodError
    end
  end

end

# my_tweet.respond_to?(:avatar) is false, lets fix it:
class Tweet
  def respond_to? method_name 
    DELEGATED_METHODS.include? method_name || super
  end
end

# my_tweet.method(:avatar) raises NoMethodError, ruby 1.9.3 can fix it:
class Tweet
  def respond_to_missing? method_name # instead of above code with 'respond_to?'
    DELEGATED_METHODS.include? method_name || super
  end
end
```
