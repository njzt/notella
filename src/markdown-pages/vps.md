---
slug: "vps"
title: "vps"
---
user
----

```
adduser fred
adduser fred sudo
chmod u+s /usr/bin/sudo # if "must be setuid root" occurs
# add put ky to .ssh/authorized_keys, check login, then
chmod 600 .ssh/authorized_keys
PermitRootLogin no # in nano /etc/ssh/sshd_config
service ssh restart
```

mysql
-----

```
sudo apt-get update
sudo apt-get install mysql-server
mysql_secure_installation

sudo apt-get install libmysqlclient-dev  
```

### new db + user

```
create database xyzdb DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

grant usage on *.* to xyzuser@localhost identified by 'xyzpasswd';
grant all privileges on xyzdb.* to xyzuser@localhost;
```

### data export import

```
mysqldump -u useroot -p moja_db_dev moja_tabela > dump.sql
mysql -u useroot -p -D moja_db_pro --default-character-set=utf8 < dump.sql
```

### mysql backup

```
CREATE USER 'backup'@'localhost' IDENTIFIED BY 'password';
GRANT SELECT, SHOW VIEW, RELOAD, REPLICATION CLIENT, EVENT, TRIGGER ON *.* TO 'backup'@'localhost';
GRANT LOCK TABLES ON *.* TO 'backup'@'localhost';

https://gist.github.com/jeop10/3777f258584632b6271dfe6e6e16098d
```

rvm
---

```
https://rvm.io/rvm/install
rvm install 1.9.3
rvm use 1.9.3 --default
```

passenger + nginx
-----------------

```
https://www.phusionpassenger.com/library/install/nginx/install/oss/xenial/
sudo /usr/sbin/passenger-memory-stats 
```

### virtual host

```
server {
    listen 80;
    server_name yourserver.com;

    # Tell Nginx and Passenger where your app's 'public' directory is
    root /path-to-app/public;

    # Turn on Passenger
    passenger_enabled on;
    passenger_ruby /path-to-ruby;
}
```

issues?
-------

#### locale: Cannot set LC\_CTYPE to default locale: No such file or directory

```
apt-get install language-pack-en
```

bundler & capistrano
--------------------

```
https://github.com/capistrano/bundler
https://bundler.io/v1.16/guides/sinatra.html

# deploy/demo.rb
+append :linked_dirs, '.bundle'
+set :rvm_custom_path, '/usr/share/rvm'  # only needed if not detected
```

### issues

#### rake: not found

```
gem install rvm-capistrano
require 'rvm/capistrano' # in deploy.rb
```

links
-----

```
https://gist.github.com/yosukehasumi/ea882abdcdf6f17d0d12280de136dce5
https://hostovita.pl/blog/zabezpieczenie-nginx-na-ubuntu-16-04-certyfikatem-od-lets-encrypt/
```

deployment
----------

```
mkdir www-apps/myapp
```

### virtual host

```
# /etc/nginx/sites-available/myapp 
server {
    listen 80;
    server_name myapp.com;

    # Tell Nginx and Passenger where your app's 'public' directory is
    root /var/www/apps/myapp/current/public;

    # Turn on Passenger
    passenger_enabled on;
    # https://www.phusionpassenger.com/library/deploy/nginx/deploy/ruby/#determine_ruby_command
    passenger_ruby /path-to-ruby;

    error_log /var/log/nginx/myapp_error.log;
    access_log /var/log/nginx/myapp_access.log;

}
```
