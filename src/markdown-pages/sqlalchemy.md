---
slug: "sqlalchemy"
title: "SQLAlchemy"
---

# Core

## basics

### Engine

> Acts as a central source of connections to a particular database, providing both a factory as well as a holding connection pool.

```python
>>> from sqlalchemy import create_engine
>>> engine = create_engine("sqlite+pysqlite:///:memory:", echo=True, future=True)
```

### Connection

> Connection represents an open resource against the database.

#### with engine.connect()

```python
>>> from sqlalchemy import text

>>> with engine.connect() as conn:
...     result = conn.execute(text("select 'hello world'"))
   # ! not committed, when the scope of the connection is released,
   # a ROLLBACK is emitted to end the transaction
```

#### with engine.begin()

```python
>>> with engine.begin() as conn:
...     conn.execute(
...         text("INSERT INTO some_table (x, y) VALUES (:x, :y)"),
...         [{"x": 6, "y": 8}, {"x": 9, "y": 10}]
...     )
    # enclose everything inside of a transaction with COMMIT at the end,
    # assuming a successful block, or ROLLBACK in case of exception raise
```

works the same as:

```python
>>> with engine.connect() as conn:
...     conn.execute(
...         text("INSERT INTO some_table (x, y) VALUES (:x, :y)"),
...         [{"x": 1, "y": 1}, {"x": 2, "y": 4}]
...     )

```

### Session

> Session in modern SQLAlchemy emphasizes a transactional and SQL execution pattern that is largely identical to that of the Connection.

#### Session in non-ORM constructs

```python
>>> with Session(engine) as session: # Session gets new Connection from Engine
...     result = session.execute(
...         text("UPDATE some_table SET y=:y WHERE x=:x"),
...         [{"x": 9, "y":11}, {"x": 13, "y": 15}]
...     )
...     session.commit()
```

### fetching rows

```python
>>> with engine.connect() as conn:
...     result = conn.execute(text("SELECT x, y FROM some_table"))
...     for row in result:
...         print(f"x: {row.x}  y: {row.y}")
```

#### accessing Row data

##### tuple

```python
result = conn.execute(text("select x, y from some_table"))
for x, y in result:
    # ...
```

##### integer index

```python
result = conn.execute(text("select x, y from some_table"))
for row in result:
    x = row[0]
```

##### attribute name

```python
result = conn.execute(text("select x, y from some_table"))
for row in result:
    y = row.y
```

##### mappings()

```python
result = conn.execute(text("select x, y from some_table"))
for dict_row in result.mappings(): #  dictionary-like RowMapping
    y = dict_row['y']
```

### sending parameters

```python
>>> with engine.connect() as conn:
...     result = conn.execute(
...         text("SELECT x, y FROM some_table WHERE y > :y"),
...         {"y": 2}
...     )
...     for row in result:
...        print(f"x: {row.x}  y: {row.y}")
```

#### multiple

uses DBAPI feature known as cursor.executemany()

```python
>>> with engine.connect() as conn:
...     conn.execute(
...         text("INSERT INTO some_table (x, y) VALUES (:x, :y)"),
...         [{"x": 11, "y": 12}, {"x": 13, "y": 14}]
...     )
...     conn.commit()
```

### bundling parameters

```python
>>> stmt = text("SELECT x, y FROM some_table WHERE y > :y ORDER BY x, y").bindparams(y=6)
>>> with engine.connect() as conn:
...     result = conn.execute(stmt)
...     for row in result:
...        print(f"x: {row.x}  y: {row.y}")
```

## working with data

### insert with Core

[+](https://docs.sqlalchemy.org/en/14/tutorial/data_insert.html)

```python
>>> from sqlalchemy import insert
>>> stmt = insert(user_table).values(name='spongebob', fullname="Spongebob Squarepants")
>>> with engine.connect() as conn:
...     result = conn.execute(stmt)
...     conn.commit()
(1,)
```

> if only a single row is inserted, it will usually include the ability to return information about column-level default values that were generated during the INSERT of that row, most commonly an integer primary key value

```python
>>> result.inserted_primary_key
(1,)
```

#### insert multiple

```python
>>> with engine.connect() as conn:
...     result = conn.execute(
...         insert(user_table),
...         [
...             {"name": "sandy", "fullname": "Sandy Cheeks"},
...             {"name": "patrick", "fullname": "Patrick Star"}
...         ]
...     )
...     conn.commit()
```

#### insert from select

```python
>>> select_stmt = select(user_table.c.id, user_table.c.name + "@aol.com")
>>> insert_stmt = insert(address_table).from_select(
...     ["user_id", "email_address"], select_stmt
... )
>>> print(insert_stmt)
```

# ORM

## basics

### Declarative base

> Classes mapped using the Declarative system are defined in terms of a base class which maintains a catalog of classes and tables relative to that base - this is known as the declarative base class

```python
>>> from sqlalchemy.orm import declarative_base
>>> Base = declarative_base()
```

### Mapped Class

> A class using Declarative at a minimum needs a **tablename** attribute, and at least one Column which is part of a primary key

```python
>>> from sqlalchemy import Column, Integer, String
>>> class User(Base):
...     __tablename__ = 'users'
...
...     id = Column(Integer, primary_key=True)
...     fullname = Column(String)
```

```python
>>> ed_user = User(name='ed', fullname='Ed Jones')
>>> ed_user.name
'ed'
```

### MetaData

> The MetaData is a registry which includes the ability to emit a limited set of schema generation commands to the database.

```python
>>> Base.metadata.create_all(engine)
BEGIN...
CREATE TABLE users (
    id INTEGER NOT NULL,
    fullname VARCHAR,
    PRIMARY KEY (id)
)
[...] ()
COMMIT
```

### Session

> When we first set up the application, at the same level as our create_engine() statement, we define a Session class which will serve as a factory for new Session objects:

```python
>>> from sqlalchemy.orm import sessionmaker
>>> Session = sessionmaker(bind=engine)
```

> In the case where your application does not yet have an Engine

```python
>>> from sqlalchemy.orm import sessionmaker
>>> Session = sessionmaker()
>>> Session.configure(bind=engine)  # once engine is available
```

```python
>>> session = Session()
```

> The above Session is associated with our SQLite-enabled Engine, but it hasn’t opened any connections yet. When it’s first used, it retrieves a connection from a pool of connections maintained by the Engine, and holds onto it until we commit all changes and/or close the session object.

## Using the session

```python
# verbose version of what a context manager will do
with Session(engine) as session:
    session.begin()
    try:
        session.add(some_object)
        session.add(some_other_object)
    except:
        session.rollback()
        raise
    else:
        session.commit()
```

> ... use of the SessionTransaction object returned by the Session.begin() method, which provides a context manager interface for the same sequence of operations:

```python
# create session and add objects
with Session(engine) as session:
    with session.begin():
      session.add(some_object)
      session.add(some_other_object)
    # inner context calls session.commit(), if there were no exceptions
# outer context calls session.close()
```

> More succinctly, the two contexts may be combined:

```python
# create session and add objects
with Session(engine) as session, session.begin():
    session.add(some_object)
    session.add(some_other_object)
# inner context calls session.commit(), if there were no exceptions
# outer context calls session.close()
```

### sessionmaker


```python
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# an Engine, which the Session will use for connection resources, typically in module scope
engine = create_engine('postgresql://scott:tiger@localhost/')

# a sessionmaker(), also in the same scope as the engine
Session = sessionmaker(engine)

# we can now construct a Session() without needing to pass the engine each time
with Session() as session:
    session.add(some_object)
    session.add(some_other_object)
    session.commit()
# closes the session
```

>  ... it also has its own sessionmaker.begin() method, analogous to Engine.begin(), which returns a Session object and also maintains a begin/commit/rollback
```python
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# an Engine, which the Session will use for connection resources
engine = create_engine('postgresql://scott:tiger@localhost/')

# a sessionmaker(), also in the same scope as the engine
Session = sessionmaker(engine)

# we can now construct a Session() and include begin()/commit()/rollback() at once
with Session.begin() as session:
    session.add(some_object)
    session.add(some_other_object)
# commits the transaction, closes the session
```

## Quuerying


```python
>>> from sqlalchemy import select
>>> result = session.execute(select(User).order_by(User.id))
```

### Select ORM entities & attributes

```python
>>> stmt = select(User, Address).join(User.addresses).order_by(User.id, Address.id)
>>> for row in session.execute(stmt):
...    print(f"{row.User.name} {row.Address.email_address}")
```

#### individual attributes

```python
>>> result = session.execute(
...     select(User.name, Address.email_address).
...     join(User.addresses).
...     order_by(User.id, Address.id)
... )
>>> for row in result:
...     print(f"{row.name}  {row.email_address}")
```

#### groupping in bundles

```python
>>> from sqlalchemy.orm import Bundle
>>> stmt = select(
...     Bundle("user", User.name, User.fullname),
...     Bundle("email", Address.email_address)
... ).join_from(User, Address)
>>> for row in session.execute(stmt):
...     print(f"{row.user.name} {row.email.email_address}")
```

#### textual statements


```python
>>> from sqlalchemy import text
>>> textual_sql = text("SELECT id, name, fullname FROM user_account ORDER BY id")
>>> orm_sql = select(User).from_statement(textual_sql)
>>> for user_obj in session.execute(orm_sql).scalars():
...     print(user_obj)
```


```python
>>> from sqlalchemy.orm import aliased
>>> # using aliased() to select from a subquery
>>> orm_subquery = aliased(User, textual_sql.subquery())
>>> stmt = select(orm_subquery)
>>> for user_obj in session.execute(stmt).scalars():
...     print(user_obj)
```




# ORM relationships

## Setup

### One To Many

```python
class Parent(Base):
    __tablename__ = 'parent'
    id = Column(Integer, primary_key=True)
    children = relationship("Child")

class Child(Base):
    __tablename__ = 'child'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('parent.id'))
```

#### delete

Delete related addresses when deleting user.

```python
class User(Base):
    # ...
    addresses = relationship("Address", cascade="all, delete")
```

Unassign related addresses (set foreign key to NULL) when deleting user.

```python
class User(Base):
    # ...
    addresses = relationship("Address")
```

#### delete-orphaqlalchemy

Address object will be marked for deletion when it is de-associated from the user.

```python
class User(Base):
    # ...
    addresses = relationship( "Address", cascade="all, delete-orphan")
```

### Many To One

Many To One with previous One To Many

```python
class Parent(Base):
    __tablename__ = 'parent'
    id = Column(Integer, primary_key=True)
    children = relationship("Child", back_populates="parent")

class Child(Base):
    __tablename__ = 'child'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('parent.id'))
    parent = relationship("Parent", back_populates="children")
```

#### using relationship.backref

```python
class Parent(Base):
    __tablename__ = 'parent'
    id = Column(Integer, primary_key=True)
    children = relationship("Child", backref="parent")
```

### One To One

```python
class Parent(Base):
    __tablename__ = 'parent'
    id = Column(Integer, primary_key=True)

    # previously one-to-many Parent.children is now one-to-one Parent.child
    child = relationship("Child", back_populates="parent", uselist=False)

class Child(Base):
    __tablename__ = 'child'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('parent.id'))

    # many-to-one side remains
    parent = relationship("Parent", back_populates="child")
```

#### Using relationship.backref

```python
class Parent(Base):
    __tablename__ = 'parent'
    id = Column(Integer, primary_key=True)

class Child(Base):
    __tablename__ = 'child'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('parent.id'))
    parent = relationship("Parent", backref=backref("child", uselist=False))
```

### Many To Many

```python
association_table = Table('association', Base.metadata,
    Column('left_id', ForeignKey('left.id'), primary_key=True),
    Column('right_id', ForeignKey('right.id'), primary_key=True)
)

class Parent(Base):
    __tablename__ = 'left'
    id = Column(Integer, primary_key=True)
    children = relationship( "Child", secondary=association_table, back_populates="parents")

class Child(Base):
    __tablename__ = 'right'
    id = Column(Integer, primary_key=True)
    parents = relationship( "Parent", secondary=association_table, back_populates="children")
```

#### using relationship.backref

```python
association_table = Table('association', Base.metadata,
    Column('left_id', ForeignKey('left.id'), primary_key=True),
    Column('right_id', ForeignKey('right.id'), primary_key=True)
)

class Parent(Base):
    __tablename__ = 'left'
    id = Column(Integer, primary_key=True)
    children = relationship("Child", secondary=association_table, backref="parents")

class Child(Base):
    __tablename__ = 'right'
    id = Column(Integer, primary_key=True)
```

#### secondary

##### secondary as callable

```python
class Parent(Base):
    # ...
    # accepts a callable that returns the ultimate argument,
    # which is evaluated only when mappers are first used
    children = relationship("Child", secondary=lambda: association_table, backref="parents")
```

##### secondary as table name string

```python
class Parent(Base):
    # ...
    # traditional “string name of the table” is accepted as well,
    # matching the name of the table as stored in Base.metadata.tables
    children = relationship("Child", secondary="association", backref="parents")
```

### Many To Many using Association Object

```python
class Association(Base):
    __tablename__ = 'association'
    left_id = Column(ForeignKey('left.id'), primary_key=True)
    right_id = Column(ForeignKey('right.id'), primary_key=True)
    extra_data = Column(String(50))
    child = relationship("Child", back_populates="parents")
    parent = relationship("Parent", back_populates="children")

class Parent(Base):
    __tablename__ = 'left'
    id = Column(Integer, primary_key=True)
    children = relationship("Association", back_populates="parent")

class Child(Base):
    __tablename__ = 'right'
    id = Column(Integer, primary_key=True)
    parents = relationship("Association", back_populates="child")
```

#### association

```python
# create parent, append a child via association
p = Parent()
a = Association(extra_data="some data")
a.child = Child()
p.children.append(a)

# iterate through child objects via association, including association attributes
for assoc in p.children:
    print(assoc.extra_data)
    print(assoc.child)
```

### Collection techniques

#### default collection_set is list

```python
oldpost = jack.posts.filter(Post.headline=='old post').one()
jack.posts.remove(oldpost)

jack.posts.append(Post('new post'))
```

#### collection_class=set

```python
class Parent(Base):
    __tablename__ = 'parent'
    parent_id = Column(Integer, primary_key=True)

    # use a set
    children = relationship(Child, collection_class=set)

parent = Parent()
child = Child()
parent.children.add(child)
assert child in parent.children
```

## loading techniques

> The loading of relationships falls into three categories; lazy loading, eager loading, and no loading

### configuration

#### at mapping time
> The loader strategy for a particular relationship can be configured at mapping time to take place in all cases where an object of the mapped type is loaded, in the absence of any query-level options that modify it. 

```python
class Parent(Base):
    __tablename__ = 'parent'

    id = Column(Integer, primary_key=True)
    children = relationship("Child", lazy='joined') # default is "select"
```

#### per query


```python
# set children to load lazily
session.query(Parent).options(lazyload(Parent.children)).all()

# set children to load eagerly with a join
session.query(Parent).options(joinedload(Parent.children)).all()
```


#### chained loading
> the query will return Parent objects without the children collections loaded. When the children collection on a particular Parent object is first accessed, it will lazy load the related objects, but additionally apply eager loading to the subelements collection on each member of children
```python
# 1.x style
session.query(Parent).options(
    lazyload(Parent.children).
    subqueryload(Child.subelements)).all()

# 2.0 style
stmt = select(Parent).options(
      lazyload(Parent.children).
      subqueryload(Child.subelements))

result = session.execute(stmt)
```




### lazy 

> available via lazy='select' or the lazyload() option, this is the form of loading that emits a SELECT statement at attribute access time to lazily load a related reference on a single object at a time.

### joined

>  available via lazy='joined' or the joinedload() option, this form of loading applies a JOIN to the given SELECT statement so that related rows are loaded in the same result set.

### subquery

### selectin

### raise

### no loading