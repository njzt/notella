---
slug: "mongo-db"
title: "mongo db"
---
navigate
--------

```
show dbs
load('loadMovieDetailsDataset.js')
use video # => switched to db video
show collections => # movies
```

queries
-------

### scalar equality

```
{'start station name': 'Broadway & W 49 St'}
db.movieDetails.find({'awards.wins':2,'awards.nominations':2}).count() # => 12
```

### array

```
{cast: ['Jeff Bridges', 'Tim Robbins']} # exact match for array
{cast: 'Jeff Bridges'} # Jeff Bridges appears in cast[]
{'cast.0': 'Jeff Bridges'} # Jeff Bridges first in cast[]
```

### range

```
{
  tripduration: 
    { $gte: 600, $lt: 1200 }
  }
}
```

### geospatial queries

```
{
  coordinates: {
    $geoWithin: { 
      $centerSphere: [ 
        [ -66.51263180685517, 18.237305803159288 ], 
        0.017351319449645724 ]
      }
    }
  }
}
```

### projections

```
db.movieDetails.find({'genres.1':'Western'}, {title: 1, _id: 0}) # get only title, 1 excludes all other except _id
db.movieDetails.find({'genres.1':'Western'}, {type: 0, poster: 0})) # get all properties except poster & type
```

### logical operators

```
db.inventory.find({
  $and : [
    { $or : [ { price : 0.99 }, { price : 1.99 } ] },
    { $or : [ { sale : true }, { qty : { $lt : 20 } } ] }
  ]
})
db.inventory.find( { price: { $not: { $gt: 1.99 } } } )
```

populate
--------

### insertOne

```
db.movies.insertOne({title: 'Star Trek', year: 1982})
=> {
	"acknowledged" : true,
	"insertedId" : ObjectId("5ad3410132cfeafb05cafe55")
}
```

### insertMany

```
db.movies.insertMany([
  {
    _id: '001-forrest',
    title: 'Forrest gump',
  },
  {
    _id: '001-forrest',
    title: 'Forrest gump',
  },
  {
    _id: '003-king kong',
    title: 'King Kong',
  },
], {
  ordered: false # do not break inserting of first error (default ordered is true)
})
```

update
------

### updateOne

```
db.movieDetails.updateOne(
  {title:'The Martian'}, # finds first document 
  {$set: {poster: 'http://poster.com/martian.jpg'}} # $set operator overwrites value
)
=> { "acknowledged" : true, "matchedCount" : 1, "modifiedCount" : 1 }
db.movieDetails.updateOne(
  {title:'The Martian'}, 
  {$inc: {'tomato.reviews': 3, 'tomato.userReviews':25}} # see more operators https://docs.mongodb.com/manual/reference/operator/update/
)
```

### updateMany

```
db.movieDetails.updateMany({rated:null}, { $unset: {rated: ""}});
=> { "acknowledged" : true, "matchedCount" : 1601, "modifiedCount" : 1599 }
```

### upsert

```
let details = { imdb: { id: 123}, ... }
db.movieDetails.updateOne(
  {'imdb.id': details.imdb.id}, # if document exists 
  { $set: details}, # then update 
  { upsert: true} # otherwise create new
);
```

### replaceOne

```
detailDoc = db.movieDetails.findOne({'imdb.id':'123'});
detailDoc.poster = 'http://poster.com/123.jpg';
detailDoc.genres.push('Documentary');
db.movieDetails.replaceOne(
  {'imdb.id':detailDoc.imdb.id},
  detailDoc
);
```

delete
------

### deleteOne

```
db.reviews.deleteOne({_id: ObjectId('595ytuu739sdk')});
```

### deleteMany

```
db.reviews.deleteMany({reviewer_id: 998726});
```

aggregation
-----------

```
# pipelines are compositions of stages
# stages are configurable for transformations
db.userColl.aggregate([ {stage1}, {stage2}, {..stageN}], {options});

db.solarSystem.aggregate([{
    $match: {
      atmosphericComposition: { $in: [/02/] },
      meanTemperature: { $gte: -40, $lte: 40},
    }
  }, {
    $project: {
      _id: 0,
      name: 1,
      hasMoons: { $gt: ["$numberOfMoons", 0]}
    }
  }], { allowDiskUse: true}
) # => { "name": "Earth", "hasMoons" : true }

[
   {
      $project:{
         _id:0,
         titleWordsCount:{
            $size:{
               $split:[ "$title", " " ]
            }
         }
      }
   },
   {
      $match:{ titleWordsCount:1 }
   }
]

# see more https://docs.mongodb.com/manual/meta/aggregation-quick-reference/
```

### $match

```
{
   "$match":{
      "imdb.rating":{
         "$gte":7
      },
      "genres":{
         "$nin":[
            "Crime",
            "Horror"
         ]
      },
      "rated":{
         "$in":[
            "PG",
            "G"
         ]
      },
      "languages":{
         "$all":[
            "English",
            "Japanese"
         ]
      }
   }
}
https://docs.mongodb.com/manual/reference/operator/aggregation/match
```

### $project

```
db.solarSystem.aggregate([{ $project: {_id:0, name:1, "gravity.value": 1}}])
db.solarSystem.aggregate([{ $project: {_id:0, name:1, surfaceGravity: "$gravity.value"}}])
db.solarSystem.aggregate([
   {
      $project:{
         _id:0,
         name:1,
         myWeight:{
            $multiply:[
               {
                  $divide:["$gravity.value",98]
               },
               93
            ]
         }
      }
   }
])
```

### $map

```
$map: {
  input: "$writers",
  as: "writer",
  in: { # "Roberto Benigni (writer)" => "Roberto Benigni"
    $arrayElemAt: [
      { $split: [ "$$writer", " (" ] },
      0
    ]
  }
}
```

### $addFields

```
db.scores.aggregate( [
   {
     $addFields: {
       totalHomework: { $sum: "$homework" } ,
       totalQuiz: { $sum: "$quiz" }
     }
   },
   {
     $addFields: { totalScore:
       { $add: [ "$totalHomework", "$totalQuiz", "$extraCredit" ] } }
   }
] )
```

### $geoNear

```
db.places.aggregate([
   {
     $geoNear: {
        near: { type: "Point", coordinates: [ -73.99279 , 40.719296 ] },
        distanceField: "dist.calculated",
        maxDistance: 2,
        query: { type: "public" },
        includeLocs: "dist.location",
        num: 5,
        spherical: true
     }
   }
])
```

### $skip, $limit

```
{ $limit : 5 }
{ $skip : 5 }
```

### $sort

```
{ $sort : { age : -1, posts: 1 } }

db.solarSystem.aggregate([ # sort can take advantage of indexes if used  early in pipeline
  { $sort: { numberOfMoons: -1}}, # sort use up to 100MB of RAM by default
], { allowDiskUse: true })
```

### $count

```
db.scores.aggregate(
  [
    {
      $match: { score: { $gt: 80 } }
    },
    {
      $count: "passing_scores"
    }
  ]
) 
=> { "passing_scores" : 4 }
```

### variables

```
"$fieldName" # field path
"$$UPPERCASE" # system variable
"$$foo" # user variable
```
