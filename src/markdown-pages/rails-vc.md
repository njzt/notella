---
slug: "rails-vc"
title: "Rails VC"
---
Rendering
---------

### yield & content\_for

```
# layout:
<html>
  <%= yield :head %>
  <body>
    <%= yield %>
  </body>
</html>
# template:
<% content_for :head do %>
  <title>A simple page</title>
<% end %>
<p>Hello, Rails!</p>
```

### Partials

#### models & collections

```
<%= render @customer %> # passes @customer as customer to partial 'customer/_customer'
<%= render @customers %> # renders in loop 'customer/_customer' for each of @customers

<%= render @customers || 'no customers found' %>

<%= render [customer1, employee1, customer2, employee2] %> # will render customers using 'customer/_customer' and employees with 'employee/_employee'

<%= render :partial => @products, :spacer_template => "product_ruler" %> # renders '_product_ruler' between @products
```

#### local variables

```
<%= render :partial => "form", :locals => { :zone => @zone } %> # => @zone passed to local variable zone
<%= render :partial => "customer", :object => @new_customer %> # => using :objects makes local variable named the same as partial
<%= render :partial => 'products', :collection => @products, :as => :item, :locals => {:title => "Products Page"} %> # each of products available as variable item
```
