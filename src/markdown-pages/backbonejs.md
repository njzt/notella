---
slug: "backbonejs"
title: "backbone.js"
---
Model
-----

```
var TodoItem = Backbone.Model.extend({});
var todoItem = new TodoItem({ description: 'Lets do it'});
todoItem.get('description'); // => 'Lets do it'
todoItem.attributes() // => {description: 'Lets do it', ...}
```

### Server

#### simple fetch

```
todoItem.url = '/todo';
todoItem.fetch(); // loads JSON from /todo url
```

#### urlRoot & REST

```
var TodoItem = Backbone.Model.extend({urlRoot: '/todos'}); // RESTful webservice
var todoItem = new TodoItem({ id: 1});
todoItem.fetch(); // request json from GET /todos/1

todoItem.set({description:'come on'});
todoItem.save // sends json to PUT /todos/1

var todoItem = new TodoItem({description:'lets go honey'});
todoItem.save(); // sends json to POST /todos
todoItem.get('id') // => 2; id from server

todoItem.destroy() // DELETE /todos/2
```

#### readonly

```
var TodoItem = Backbone.Model.extend({
  //...
  sync: function(method, model, options){ // methods: read, create, update or delete
    if (method === 'read') {
      Backbone.sync(method, model, options);
    } else {
      console.error('You cannot ' + method);
    }
  } 
});
```

### Server API adapters

#### response

```
// {todo: {desc: 'buy milk', status: 'incomplete', id: 2}}
var TodoItem = Backbone.Model.extend({
  parse: function(response){
    response = response.todo;
    response.description = response.desc;
    delete response.desc
    return response;
  }
});
```

#### parse on constructor

```
todoItem = new TodoItem(
  {todo: {desc: 'buy milk', status: 'incomplete', id: 2}},
  {
    parse: true
  }
)
```

#### requests

```
// {todo: {desc: 'buy milk', status: 'incomplete', id: 2}}
var TodoItem = Backbone.Model.extend({
  toJSON: function(){
    var attrs = _.clone(this.attributes);
    attrs.desc = attrs.description;
    delete attrs.description;
    return {todo: attrs};
  }
});
```

#### idAttribute

```
var TodoItem = Backbone.Model.extend({
  idAttribute: '_id' 
});
```

### Default values

```
var TodoItem = Backbone.Model.extend({
  defaults : {
    status : 'incomplete'
  }
})

var TodoItem = Backbone.Model.extend({
  defaults : function() {
    return { createdAt : new Date() }
  }
})
```

### Events

```
todoItem.on('change', function(){
  ...
});

todoItem.set({description: 'buy milk'}) // triggers 'change' event
todoItem.set({description: 'buy milk'},{silent:true}) // does not trigger 'change' event

todoItem.off('change') // removes event listener
```

#### built-in

```
change
change:attr
destroy
sync
error
all // any event
```

#### custom

```
todoItem.on('event-name', function(){...});
todoItem.trigger('event-name');
```

View
----

### el & $el

```
var SimpleView = Backbone.View.extend({});
var simpleView = new SimpleView();
simpleView.el // => <div></div>
simpleView.$el // the same as $(simpleView.el)
```

#### custom

```
var TodoView = Backbone.View.extend({
  tagName: 'article',
  id: 'todo-view',
  className: 'todo'
});

var todoView = new TodoView();
todoView.el // => <article id="todo-view" class="todo"></article>
```

#### use existing via initialization

```
var todoView = new TodoView({
  model: todoItem,
  el: $('.todo-wrapper'),
})
```

### render

```
var TodoView = Backbone.View.extend({
  render: function(){
    var html = '<h3>' + this.model.get('description') + '</h3>';
    this.$el.html(html);
  }
});
```

#### underscore

```
var TodoView = Backbone.View.extend({
  template : _.template('<h3><%= description %></h3>'),
  render: function(){
    var attributes = this.model.attributes();
    this.$el.html(this.template(attributes));
  }
});
```

### model sync

```
var TodoView = Backbone.View.extend({
  // ...
  initialize : function(){
    this.model.on('change', this.render, this);
    this.model.on('destroy', this.remove, this);
  },
  remove : function(options){ // extra options
    if (options.highlight) {
      this.$el.highlight(); 
    }
    this.$el.remove();
  }
});

//later
todoItem.destroy({highlight: true}) // pass options to callback
```

#### listenTo

```
// alternative to attaching events on this.model
var TodoView = Backbone.View.extend({
  initialize : function(){
    this.listenTo(this.model,'change', this.render);
    this.listenTo(this.model,'destroy', this.remove);
  },
});

//later
todoView.stopListening();
todoView.remove();
```

### events

```
var TodoView = Backbone.View.extend({
  // ...
  events : {
    'click h3' : 'alertStatus', // '<event> <selector> : <method>'
    'dblclick' : 'open', // no selector means events anywhere in $el
    'mouseover title date' : 'showTooltip'
   },
  alertStatus : function(e) {
    alert(this.model.get('status'));
  }
});
```

### form

```
var TodoForm = Backbone.View.extend({
  //...
  events: {
    submit: 'save'
  },
  save: function(e){
    e.preventDefault();
    var newDescription = this.$('input[name=description]').val();
    this.model.save({description: newDescription},{
      success: function(model, response, options){
        Backbone.history.navigate('',{trigger: true});
      },
      error: function(model, xhr, options){
        var errors = JSON.parse(xhr.responseText).errors;
        alert('Ooops', errors);
      }
    })
  }
})
```

Collection
----------

```
var TodoList = Backbone.Collection.extend({
  model: TodoItem
});
var todoList = new TodoList();

todoList.length;

todoList.at(0); // by index
todoList.get(1); // by id
```

### populate

```
todoList.add(todoItem);
todoList.remove(todoItem);
```

#### reset

```
var todos = [
  {description: 'Buy milk'},
  {description: 'Take powerball break'}
];
todoList.reset(todos);
```

#### fetch

```
var TodoList = Backbone.Collection.extend({
  model: TodoItem,
  url: '/todos' 
});
var todoList = new TodoList();
todoList.fetch() // GET /todos
```

#### comparator

```
var TodoList = Backbone.Collection.extend({
  comparator: 'status' // using attribute
});

var TodoList = Backbone.Collection.extend({
  comparator: function(item1, item2) { // using function
    return item1.get('status') < item2.get('status')
  }
});

todoItems.fetch() // => items sorted by comparator
```

### events

#### built-in

```
todoList.on('reset', callback); // triggered by 'reset' and 'fetch'
todoList.on('add', function(addedItem){});
todoList.on('remove', function(removedItem){});
```

#### silent

```
todoList.fetch({silent: true}) // does not trigger 'reset'
```

#### model

```
// events triggered on model in collection triggers also on collection
change
change:attr
destroy
syncare triggered
error
all // any event
```

### iteration

```
todoList.forEach(function(todoItem){})
var descs = todoList.map(function(todoItem){ return todoItem.get('description') })
var done = todoList.filter(function(todoItem){ return todoItem.get('status') === 'complete' })
// much more from undescore.js
```

### Server API adapters

#### response

```
// {page: 2, data_records: [{...},{...}]}

var TodoItem = Backbone.Collection.extend({
  parse: function(response){
    this.page = response.page // we can store extra data if we need it
    return response.data_records 
  }
})
```

#### extra params in fetch

```
todoItems.fetch({ data: {page: 2}}) // GET /todos?page=6
```

Collection View
---------------

```
// does not render HTML itself, delegates it to views of models

var TodoListView = Backbone.Collection.extend({});
var todoListView = new TodoListView({ collection: todoList });
```

### render

```
new TodoListView({ 
  collection: todoList,
  render: function(){
    this.addAll()
  },
  addAll: function() {
    this.collection.forEach(this.addOne, this);
  },
  addOne: function(todoItem) {
    var todoView = new TodoView({model: todoItem});
    this.$el.append(todoView.render().el);
  }
});
```

### collection events

#### add & reset

```
new TodoListView({ 
  initialize: function(){
    this.collection.on('add', this.addOne, this);
    this.collection.on('reset', this.addAll, this);
  },
  //...
});
```

#### fixing remove with Custom Events

```
//collection TodoList, NOT collection view!
initialize: function(){
  this.on('remove', this.hideModel);
},
hideModel: function(todoItem) {
  todoItem.trigger('hide');
}
//view TodoItem
initialize: function(){
  this.model.on('hide', this.remove, this);
}
```

Router & History
----------------

```
var routes = Backbone.Router.extend({
  routes: {
    "": index,
    "todos/:id": show 
  },
  index: function(){
    this.todoList.fetch();
  },
  show: function(id){
    this.todoList.focusOnTodoItem(id);
  },
  initialize: function(){
    this.todoList = this.options.todoList
  }
});

var todoRouter = new routes({todoList: todoList});
```

### route matchers

```
'search/:query' // search/ruby => query = 'ruby'
'search/:query/p:page' // search/ruby/p2 => query = 'ruby', page = 2
'folder/:name-:mode' // folder/foo-r => name = 'foo', mode = 'r'
'file/*path' // file/hello/world.txt => path = 'hello/world.txt'
```

#### optional

```
'search/:query(/p:page)(/)' // search/milk, search/milk/2 or search/milk/2/
```

#### regex

```
// cant be in routes object, must be defined inside router initializer
initialize: function(){
  this.route(/^todos\/(\d+)$/, 'show')
}
```

### navigate

```
router.navigate('todos/1',{
  trigger: true // required to trigger action, not only update url
})
```

### history

```
Backbone.history.start();
router.navigate('/todos/1') // => #/todos/1

Backbone.history.start({pushState: true});
router.navigate('/todos/1') // => /todos/1
```

### simple app

```
var TodoApp = new (Backbone.Router.extend({
  routes: { "": "index", "todos/:id": "show" },
  initialize: function(){
    this.todoList = new TodoList();
    this.todosView = new TodoListView({collection: this.todoList});
    $('#app').append(this.todosView.el);
  },
  start: function(){
    Backbone.history.start({pushState: true});
  },
  index: function(){
    this.todoList.fetch();
  },
  show: function(id){
    this.todoList.focusOnTodoItem(id);
  }
}));

$(document).ready(function(){
  TodoApp.start();
})
```

App organization
----------------

### namespaces

```
var App = {
  Model: {},
  Collection: {},
  View: {}
}
App.TodoRouter = Backbone.Router.extend({...}) // single router per app

//later
App.Model.TodoItem = Backbone.Model.extend({...});
var todoItem = new App.Model.TodoItem({...});
```

### handle links

```
// take care of links outside backbone views
var App = {
  //...
  start: function(){
    $('a[data-internal]').click(function(e){
      e.preventDefault();
      Backbone.history.navigate(e.target.pathname, {trigger: true});
    });
    Backbone.history.start({pushState: true})
  }
}

App.start();
```

### App as View

```
var AppointmentApp = new (Backbone.View.extend({
  Collections: {},
  Models: {},
  Views: {},
  events: {
    'click a[data-backbone]': function(e){
      e.preventDefault();
      Backbone.history.navigate(e.target.pathname, { trigger: true });
    }
  },
  start: function(data){
    this.appointments = new AppointmentApp.Collections.Appointments(data.appointments);
    var appointmentsView = new AppointmentApp.Views.Appointments({collection: this.appointments});
    $('#app').html(appointmentsView.render().el);
  }
}))({el: document.body});
```
