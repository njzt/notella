---
slug: "jquery"
title: "jQuery"
---
### DOM

#### get & set on node

```
el.text();
el.text('new content')

el.data('price') // => value of 'data-price' attribute
el.data('price',199)

form.attr('action');

el.addClass('highlight');
el.removeClass('highlight');
el.toggleClass('highlight');
el.hasClass('highlight');

el.css('display')
el.css('display','none')
el.css({'display':'none','color':'#fff'}) // set many at once
```

#### traversing

```
$("#destinations").find("li") // $("#destinations li") 
$("#destinations").children("li") // $("#destinations > li") 
$("li").first() // $("li:first")

$("li").first().next()
$("#destinations").parent()
$("#destinations").closest(".wrapper") // closest parent, 0 or 1 node
```

#### filtering

```
$(".vacation").filter(".last-minute") // limits .vacation to matching .last-minute
```

#### manipulating

```
// append to DOM
container.append(element) // element.appendTo(container); at the bottom
container.prepend(element) // element.prependTo(container); at the top 

container.before(element) // element.beforeTo(container)
container.after(element) // element.afterTo(container)

// detach
element.detach() // removes from DOM preserving all data and events

// remove
element.remove()
```

### event handlers

#### events

```
//mouse
click
dblclick

focusin
focusout

mousedown
mouseup
mouseover
mouseout
mouseenter
mouseleave
mousemove

//keyboard
keyup
keypress
keydown

//form
blur
focus
change
submit
select
```

#### namespacing & detaching

```
$('button').on('click.image', ... ) // .image namespace

$('button').off('click') // detach all click events
$('button').off('click.image') // detach click events only from .image namespace
$('button').off('.image') // detach all button events in .image namespace
```

#### triggering

```
$('button').trigger('click')
$('button').trigger('click.image')
```

#### this

```
$(".removeMe").on("click", function(){
  $(this).remove(); # 'this' is event target
});
```

#### delegation with selector

```
$(".vacation").on('click','button',function(){ // delegates to button inside .vacation
})
```

#### stop & prevent

```
event.stopPropagation();
event.preventDefault();
```

### effects

```
element.slideUp(); // slideDown(); slideToggle();
element.fadeIn(); // fadeOut(); fadeToggle();
```

#### animate

```
element.animate({'top' : '-10px'}) // 400 ms
element.animate({'top' : '-10px'}, 'fast') // 200 ms
element.animate({'top' : '-10px'}, 600) // 'slow' 
```

### Ajax

#### get

```
$.ajax('/url',{
  success : function(response){
    // handle response
  },
  data : {
    me : 'boss'
  }
})

// shorter
$.get('/url?me=boss', function(response){
  // handle response
})
```

#### post & form serialize

```
$("#form").on('submit', function(event){
  event.preventDefault();
  var form = $(this);
  $.ajax(form.attr('action'), {
    type : 'post',
    data : form.serialize() # handles all form fields
  })
})
```

#### json

```
$.ajax('/books', {
  dataType : 'json', // parse response as json
  contentType : 'application/json' // optional, ask server to respond with json
  success : function(resObject){ }
})

//shorter

$.getJSON('/books', function(resObject){ })
```

#### errors & timeout

```
$.ajax('/url',{
  success : function(response){},
  error : function(request, errorType, errorMessage) {
  },
  timeout : 3000 // miliseconds
})
```

#### before & after

```
$.ajax('/url',{
  success : function(response){},
  beforeSend : function() {},
  complete : function(){} // after both success & error
})
```

#### context

```
$.ajax('/url',{
  context : krissKross, // 'this' points to context inside callbacks
  success : function(response){
    this.jump() // krissKross jumps :)
  }
})
```

### Promises

```
var weatherToday = function(city){ // DRY ajax method
  var promise = $.ajax('/get/weather',{ // missing success callback
    data : { where : city }
  });
  return promise;
}

var todayPromise = weatherToday('bratislava');
todayPromise.done(function(result){}) // successcallback is here
```

#### resolve & reject

```
var weatherToday = function(city){
  var promise = $.Deferred();
  $.ajax('/get/weather',{
    data : { where : city },
    success : function(result) {
      promise.resolve(result.weather) // triggers promise.done callback
    },
    error : function() {
      promise.reject() // triggers promise.fail callback
    }

  });
  return promise;
}
```

#### when & then

```
$.when(
  promise1, 
  promise2
).then(function(promise1result, promise2result){
})
```

### Utility methods

```
$.each(collection, function(index,object){ })
$.map(collection, function(item,index){ })

$.extend({days:3, nights:3},{days:4})
```

### Plugin

```
$.fn.photofy = function(options){ // makes method available for jQuery objects
  this.each(function(){
    var element = this;
    ...
  })
}
$('.photo-wrapper').photofy({blur:true});
```
