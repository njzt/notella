---
slug: "ruby-testing"
title: "RUBY testing"
---
Rails Testing
-------------

### Test::Unit

#### Test::Unit::TestCase

```
#boolean_test.rb
require 'test/unit'
class BooleanTest < Test::Unit::TestCase
  def test_true_is_true
    assert true, "True should be truthy"
  end
end
```

#### assertions

```
assert <conditional>
assert_equal <expected>,<actual> # !assert_not_equal
assert_nil <object> # !assert_not_nil
assert_match <pattern>, <string> # !assert_no_match

assert_raise <error> <block which raises error>

assert_respond_to <object>, :<method>
assert_kind_of <Class>, <object>
```

### Model testing

#### ActiveSupport::TestCase

```
# test/unit/zombie.test
require 'test_helper'

class ZombieTest < ActiveSupport::TestCase
  test "invalid without a name" do # def test_invalid_without_a_name
    z = Zombie.new
    assert z
  end
end
```

#### Rails rake tasks

```
rake db:test:prepare # check for migrations & load schema
rake test # db:test:prepare, then run all tests

ruby -Itest test/unit/zombie_test.rb # single test
ruby -Itest test/unit/zombie_test.rb -n test_the_truth # single test case
```

#### fixtures

```
#/test/fixtures/zombie.yml
ash:
  name: Ash

# after each test case test db is rolled back to initial fixtures state
z = zombies(:ash)
```

### DRY tests

#### setup

```
def setup # run before every test case
  @zombie = zombies(:ash) 
end
```

#### test\_helper

```
# /test/test_helper.rb
class ActiveSupport::TestCase
  def assert_presence(model, field)
    model.valid?
    assert_match /can't be blank/, model.errors[field].join,
      "Presence error for #{field} not found on #{model.class}"
  end
end
```

#### shoulda

```
should ensure_length_of(:password).
              is_at_least(6).
              is_at_most(20)
```

### Mocks and stubs

```
group :test do
  gem 'mocha'
end
```

#### stubs & expects

```
def decapitate
  weapon.slice(self, :head)
  self.status = 'dead again'
end

# stub - for replacing a method with code that returns a specified result
test "decapitate should set status to dead again" do
  @zombie.weapon.stubs(:slice) # mock the slice
  @zombie.decapitate # safely calls mocked weapon.slice from inside
  assert "dead again", @zombie.status
end

# mock - a stub with an assertion that the method gets called
test "decapitate should call slice" do 
  @zombie.weapon.expects(:slice) # mock the slice & assert that it gets called
  @zombie.decapitate
end
```

#### returning proper results

```
def geolocate
  loc = Zoogle.graveyard_locator(self.graveyard)
  "#{loc[:latt]}, #{loc[:long]}"
end

test "geolocate returns properly formatted lat, long" do
  Zoogle.stubs(:graveyard_locator)
        .with(@zombie.graveyard)
        .returns({latt: 3, long: 2})

  assert_equal "2, 3", @zombie.geolocate
end
```

#### Object Stub

```
def geolocate_with_object
  loc = Zoogle.graveyard_locator(self.graveyard)
  "#{loc.latt}, #{loc.long}"
end

test "geolocate returns properly formatted lat, long" do
  loc = stub(latt: 2, long: 3)
  Zoogle.stubs(:graveyard_locator)
        .returns(loc)

  assert_equal "2, 3", @zombie.geolocate_with_object
end
```

### integration testing

```
rails g integration_test zombies
# /test/integration/zombies_test.rb
require 'test_helper'
class ZombieTest < ActionDispatch::IntegrationTest
  fixtures :all
  test 'the truth' do
    assert true
  end
end
```

#### assertions

```
assert_response :success # assert_response 200
assert_redirected_to root_url
assert_tag 'a', attributes: {href: root_url}, # DOM elements
assert_select 'h1', 'Twitter for Zombies' # css selector with optional content inside
```

#### capybara

```
group :test do
  capybara
end

#/test/test_helper.rb
require 'capybara/rails'
class ActionDispatch::IntegrationTest
  include Capybara::DSL
  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end
end
```

#### Capybara navigation

```
visit root_url

click_link 'text_or_id' # click_button, click_on

# for form elements first arg is form label, element name or id
fill_in 'First Name', with: 'John'
choose 'a radio button'
check 'a checkbox' # uncheck
attach_file 'Image', '/path/to/file.jpg'
select 'Option', from: 'Select Box'

current_url
current_path
```

#### Capybara checks

```
within "h1" do
  assert has_content? zombie.name # has_no_content?
end

has_selector? "#zombie1 h1"
has_selector? "#zombie1 h1", text: 'Ash' # count: 5, visible: true

has_link?
has_field?
has_css?
has_xpath?
```

### Factories

```
group :development, :test do
  gem 'factory_girl_rails'
end
```

#### actions

```
Factory(:zombie) # or FactoryGirl.create(:zombie); returns new saved Zombie
FactoryGirl.build(:zombie) # returns new unsaved Zombie
FactoryGirl.attributes_for(:zombie) # returns hash of attributes

Factory(:zombie, :name => 'Ash2') # set attribute 
```

#### /test/factories/zombies.rb

```
FactoryGirl.define do
  factory :zombie do
    name 'Ash'
    graveyard 'Oak Park'

    factory :zombie_bill do
      name 'Bill'
    end
  end
end
```

#### sequence

```
FactoryGirl.define do
  factory :zombie do
    sequence(:name) {|i| "Ash #{i}"}
    graveyard 'Oak Park'
  end
end
```

#### association

```
FactoryGirl.define do
  factory :weapon do
    name 'Broadsword'
    association :zombie # refers to Factory of :zombie
  end
end

association :weapon, factory: :hatchet # specify concrete factory in weapon
```

#### association in tests

```
zombie = Factory(:zombie)
tweet = Factory(:tweet, zombie: zombie)
```
