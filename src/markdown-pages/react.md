---
slug: "react"
title: "React"
---

# basics

## JSX

```javascript
const name = "Josh Perez"
const element = <h1>Hello, {name}</h1>

ReactDOM.render(element, document.getElementById("root"))
```

### attributes

```javascript
const element = <img src={user.avatarUrl} /> // expressions
const element = <div tabIndex="0" /> // strings
```

### injection attacks

```javascript
const title = response.potentiallyMaliciousInput
// This is safe:
const element = <h1>{title}</h1>
```

### React.createElement()

```javascript
const element = <h1 className="greeting">Hello, world!</h1>
// is same as:
const element = React.createElement(
  "h1",
  { className: "greeting" },
  "Hello, world!"
)
// which creates object like (it is simplified):
const element = {
  type: "h1",
  props: {
    className: "greeting",
    children: "Hello, world!",
  },
}
```

[Add React to website](https://reactjs.org/docs/add-react-to-a-website.html)

### function vs class

```javascript
function Welcome(props) {
  return <h1>Hello, {props.name}</h1>
}
// or
class Welcome extends React.Component {
  render() {
    return <h1>Hello, {this.props.name}</h1>
  }
}
```

### props are read-only

All React components must act like pure functions with respect to their props.

## Lifecycle

### componentDidMount

runs after the component output has been rendered to the DOM

```javascript
componentDidMount() {
  this.timerID = setInterval(
    () => this.tick(),
    1000
  );
}
```

### componentWillUnmount

invoked immediately before a component is unmounted and destroyed

```javascript
componentWillUnmount() {
  clearInterval(this.timerID);
}
```

## State

### Do Not Modify State Directly

```javascript
// Wrong
this.state.comment = 'Hello

// Correct
this.setState({comment: 'Hello'});
```

### State Updates May Be Asynchronous

```javascript
// Wrong
this.setState({
  counter: this.state.counter + this.props.increment,
})
// Correct
this.setState((state, props) => ({
  counter: state.counter + props.increment,
}))
```

### State Updates are Merged

```javascript
 constructor(props) {
    super(props);
    this.state = { posts: [], comments: [] };
  }

  componentDidMount() {
    fetchPosts().then(response => {
      this.setState({ posts: response.posts });
    });

    fetchComments().then(response => {
      this.setState({ comments: response.comments });
    });
  }
```

## Events

### HTML vs React

```javascript
// HTML
<button onclick="activateLasers()">Activate</button>
// React
<button onClick={activateLasers}>Activate</button>
```

### 'this' in callbacks

using bind

```javascript
class MyComponent extends React.Component {
  constructor(props) {
    super(props)
    this.callOnClick = this.callOnClick.bind(this)
  }
  callOnClick() {
    console.log("Button is clicked")
  }
  render() {
    return <button onClick={this.callOnClick}> Click me!</button>
  }
}
```

arrow functions

```javascript
export default class MyComponent extends React.Component {
  callOnClick(e) {
    console.log("Button is clicked", e.target.name)
  }
  render() {
    return (
      <button name="MyButton" onClick={e => this.callOnClick(e)}>
        Click me!
      </button>
    )
  }
}
```

### Pass argument to event handler

```javascript
<button onClick={(e) => this.deleteRow(id, e)}>Delete Row</button>
<button onClick={this.deleteRow.bind(this, id)}>Delete Row</button>
```

## Conditional Rendering

### if-else

```javascript
function Greeting(props) {
  const isLoggedIn = props.isLoggedIn
  if (isLoggedIn) {
    return <UserGreeting />
  }
  return <GuestGreeting />
}
```

### element variables

```javascript
 render() {
    const isLoggedIn = this.state.isLoggedIn;
    let button;
    if (isLoggedIn) {
      button = <LogoutButton onClick={this.handleLogoutClick} />;
    } else {
      button = <LoginButton onClick={this.handleLoginClick} />;
    }

    return (
      <div>
        <Greeting isLoggedIn={isLoggedIn} />
        {button}
      </div>
    );
  }
```

### && operator

```javascript
function Mailbox(props) {
  const unreadMessages = props.unreadMessages
  return (
    <div>
      <h1>Hello!</h1>
      {unreadMessages.length > 0 && (
        <h2>You have {unreadMessages.length} unread messages.</h2>
      )}
    </div>
  )
}
```

### null

```javascript
function WarningBanner(props) {
  if (!props.warn) {
    return null
  }

  return <div className="warning">Warning!</div>
}
```

## Lifting state up

There should be a single “source of truth” for any data that changes in a React application. Usually, the state is first added to the component that needs it for rendering. Then, if other components also need it, you can lift it up to their closest common ancestor. Instead of trying to sync the state between different components, you should rely on the top-down data flow.

## Composition

React has a powerful composition model, and we recommend using composition instead of inheritance to reuse code between components.

### props.children

```javascript
function FancyBorder(props) {
  return (
    <div className={"FancyBorder FancyBorder-" + props.color}>
      {props.children}
    </div>
  )
}

function WelcomeDialog() {
  return (
    <FancyBorder color="blue">
      <h1 className="Dialog-title">Welcome</h1>
      <p className="Dialog-message">Thank you for visiting our spacecraft!</p>
    </FancyBorder>
  )
}
```

### React elements as props

```javascript
function SplitPane(props) {
  return (
    <div className="SplitPane">
      <div className="SplitPane-left">{props.left}</div>
      <div className="SplitPane-right">{props.right}</div>
    </div>
  )
}

function App() {
  return <SplitPane left={<Contacts />} right={<Chat />} />
}
```

### Specialization

Sometimes we think about components as being “special cases” of other components.

```javascript
function Dialog(props) {
  return (
    <FancyBorder color="blue">
      <h1 className="Dialog-title">{props.title}</h1>
      <p className="Dialog-message">{props.message}</p>
    </FancyBorder>
  )
}

function WelcomeDialog() {
  return (
    <Dialog title="Welcome" message="Thank you for visiting our spacecraft!" />
  )
}
```

## Fragment

#### Short syntax

You can use <></> the same way you’d use any other element except that it doesn’t support keys or attributes.

```javascript
class Columns extends React.Component {
  render() {
    return (
      <>
        <td>Hello</td>
        <td>World</td>
      </>
    );
  }
}
```
#### Keyed Fragments

```javascript
function Glossary(props) {
  return (
    <dl>
      {props.items.map(item => (
        // Without the `key`, React will fire a key warning
        <React.Fragment key={item.id}>
          <dt>{item.term}</dt>
          <dd>{item.description}</dd>
        </React.Fragment>
      ))}
    </dl>
  );
}
```

## HOC 
A higher-order component is a function that takes a component and returns a new component.

#### Don’t Mutate the Original Component. 
Use Composition.

```javascript
function logProps(WrappedComponent) {
  return class extends React.Component {
    componentDidUpdate(prevProps) {
      console.log('Current props: ', this.props);
      console.log('Previous props: ', prevProps);
    }
    render() {
      // Wraps the input component in a container, without mutating it. Good!
      return <WrappedComponent {...this.props} />;
    }
  }
}
```

#### Pass Unrelated Props Through to the Wrapped Component
HOCs add features to a component. It’s expected that the component returned from a HOC has a similar interface to the wrapped component.


```javascript
render() {
  // Filter out extra props that are specific to this HOC and shouldn't be
  // passed through
  const { extraProp, ...passThroughProps } = this.props;

  // Inject props into the wrapped component. These are usually state values or
  // instance methods.
  const injectedProp = someStateOrInstanceMethod;

  // Pass props to wrapped component
  return (
    <WrappedComponent
      injectedProp={injectedProp}
      {...passThroughProps}
    />
  );
}
```

#### Wrap the Display Name for Easy Debugging

```javascript
function withSubscription(WrappedComponent) {
  class WithSubscription extends React.Component {/* ... */}
  WithSubscription.displayName = `WithSubscription(${getDisplayName(WrappedComponent)})`;
  return WithSubscription;
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
```




## Refs and DOM

There are a few good use cases for refs:

- Managing focus, text selection, or media playback.
- Triggering imperative animations.
- Integrating with third-party DOM libraries.

### createRef

#### Adding a Ref to a DOM Element

```javascript
class CustomTextInput extends React.Component {
  constructor(props) {
    super(props)
    // create a ref to store the textInput DOM element
    this.textInput = React.createRef()
    this.focusTextInput = this.focusTextInput.bind(this)
  }

  focusTextInput() {
    // Explicitly focus the text input using the raw DOM API
    // Note: we're accessing "current" to get the DOM node
    this.textInput.current.focus()
  }

  render() {
    // tell React that we want to associate the <input> ref
    // with the `textInput` that we created in the constructor
    return (
      <div>
        <input type="text" ref={this.textInput} />
        <input
          type="button"
          value="Focus the text input"
          onClick={this.focusTextInput}
        />
      </div>
    )
  }
}
```

#### Adding a Ref to a Class Component

You may not use the ref attribute on function components because they don’t have instances.

```javascript
class AutoFocusTextInput extends React.Component {
  constructor(props) {
    super(props)
    this.textInput = React.createRef()
  }

  componentDidMount() {
    this.textInput.current.focusTextInput()
  }

  render() {
    return <CustomTextInput ref={this.textInput} />
  }
}
```

### Callback Refs

#### DOM Element

```javascript
class CustomTextInput extends React.Component {
  constructor(props) {
    super(props)

    this.textInput = null

    this.setTextInputRef = element => {
      this.textInput = element
    }

    this.focusTextInput = () => {
      // Focus the text input using the raw DOM API
      if (this.textInput) this.textInput.focus()
    }
  }

  componentDidMount() {
    // autofocus the input on mount
    this.focusTextInput()
  }

  render() {
    // Use the `ref` callback to store a reference to the text input DOM
    // element in an instance field (for example, this.textInput).
    return (
      <div>
        <input type="text" ref={this.setTextInputRef} />
        <input
          type="button"
          value="Focus the text input"
          onClick={this.focusTextInput}
        />
      </div>
    )
  }
}
```

#### Class Component

```javascript
function CustomTextInput(props) {
  return (
    <div>
      <input ref={props.inputRef} />
    </div>
  )
}

class Parent extends React.Component {
  render() {
    return <CustomTextInput inputRef={el => (this.inputElement = el)} />
  }
}
```

## Forwarding Refs

Can be inconvenient for highly reusable “leaf” components like FancyButton or MyTextInput. These components tend to be used throughout the application in a similar manner as a regular DOM button and input, and accessing their DOM nodes may be unavoidable for managing focus, selection, or animations.

```javascript
const FancyButton = React.forwardRef((props, ref) => (
  <button ref={ref} className="FancyButton">
    {props.children}
  </button>
))

// You can now get a ref directly to the DOM button:
const ref = React.createRef()
;<FancyButton ref={ref}>Click me!</FancyButton>
```

### Forwarding refs in HOC
We can explicitly forward refs to the inner FancyButton component using the React.forwardRef API

```javascript
function logProps(Component) {
  class LogProps extends React.Component {
    componentDidUpdate(prevProps) {
      console.log("old props:", prevProps)
      console.log("new props:", this.props)
    }

    render() {
      const { forwardedRef, ...rest } = this.props

      // Assign the custom prop "forwardedRef" as a ref
      return <Component ref={forwardedRef} {...rest} />
    }
  }

  // Note the second param "ref" provided by React.forwardRef.
  // We can pass it along to LogProps as a regular prop, e.g. "forwardedRef"
  // And it can then be attached to the Component.
  return React.forwardRef((props, ref) => {
    return <LogProps {...props} forwardedRef={ref} />
  })
}
```
# Context

Context is designed to share data that can be considered “global” for a tree of React components, such as the current authenticated user, theme, or preferred language.

```javascript
// Context lets us pass a value deep into the component tree
// without explicitly threading it through every component.
// Create a context for the current theme (with "light" as the default).
const ThemeContext = React.createContext("light")
```

## providing

```javascript
class App extends React.Component {
  render() {
    // Use a Provider to pass the current theme to the tree below.
    // Any component can read it, no matter how deep it is.
    // In this example, we're passing "dark" as the current value.
    return (
      <ThemeContext.Provider value="dark">
        <Toolbar />
      </ThemeContext.Provider>
    )
  }
}
```

## consuming

### static contextType

In class-based components:

```javascript
class Image extends React.Component {
  // alternative to: "Image.contextType = ThemeContext"
  // static contextType = ThemeContext,
  render() {
    const theme = this.context
    return (
      <div className={`${theme}-image image`}>
        <div className={`${theme}-ball ball`} />
        <Button />
      </div>
    )
  }
}

Image.contextType = ThemeContext
```

### Context.Consumer

In functional components:

```javascript
function Image(props) {
  // We don't need this anymore
  // const theme = this.context

  return (
    <ThemeContext.Consumer>
      {theme => (
        <div className={`${theme}-image image`}>
          <div className={`${theme}-ball ball`} />
          <Button />
        </div>
      )}
    </ThemeContext.Consumer>
  )
}
```

## Hooks

### State Hook 


```javascript
import React, { useState } from 'react';

function Example() {
  // Declare a new state variable, which we'll call "count"
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div>
  );
}
```

#### multiple

```javascript
function ExampleWithManyStates() {
  // Declare multiple state variables!
  const [age, setAge] = useState(42);
  const [fruit, setFruit] = useState('banana');
  const [todos, setTodos] = useState([{ text: 'Learn Hooks' }]);
  // ...
}
```
### Effect Hook
The Effect Hook, `useEffect`, adds the ability to perform side effects from a function component. 

```javascript
import React, { useState, useEffect } from 'react';

function Example() {
  const [count, setCount] = useState(0);

  // Similar to componentDidMount and componentDidUpdate:
  useEffect(() => {
    // Update the document title using the browser API
    document.title = `You clicked ${count} times`;
  });

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div>
  );
}
```

When you call useEffect, you’re telling React to run your “effect” function after flushing changes to the DOM. Effects are declared inside the component so they have access to its props and state. By default, React runs the effects after every render — including the first render. 

#### cleanup 

Effects may also optionally specify how to “clean up” after them by returning a function. 

```javascript
import React, { useState, useEffect } from 'react';

function FriendStatus(props) {
  const [isOnline, setIsOnline] = useState(null);

  function handleStatusChange(status) {
    setIsOnline(status.isOnline);
  }

  useEffect(() => {
    ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
    return () => {
      ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
    };
  });

  if (isOnline === null) {
    return 'Loading...';
  }
  return isOnline ? 'Online' : 'Offline';
}
```

### Building hooks 

First, we’ll extract this logic into a custom Hook called `useFriendStatus`, it takes friendID as an argument, and returns whether our friend is online.
:

```javascript
import React, { useState, useEffect } from 'react';

function useFriendStatus(friendID) {
  const [isOnline, setIsOnline] = useState(null);

  function handleStatusChange(status) {
    setIsOnline(status.isOnline);
  }

  useEffect(() => {
    ChatAPI.subscribeToFriendStatus(friendID, handleStatusChange);
    return () => {
      ChatAPI.unsubscribeFromFriendStatus(friendID, handleStatusChange);
    };
  });

  return isOnline;
}
```

Now we can use it from both components:

```javascript
// FriendStatus
function FriendStatus(props) {
  const isOnline = useFriendStatus(props.friend.id);

  if (isOnline === null) {
    return 'Loading...';
  }
  return isOnline ? 'Online' : 'Offline';
}

// FriendListItem
function FriendListItem(props) {
  const isOnline = useFriendStatus(props.friend.id);

  return (
    <li style={{ color: isOnline ? 'green' : 'black' }}>
      {props.friend.name}
    </li>
  );
}
```






