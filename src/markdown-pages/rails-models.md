---
slug: "rails-models"
title: "Rails Models"
---
Migrations
----------

### data definition tasks

```
create_table(name, options)
drop_table(name)
rename_table(old_name, new_name)

add_column(table_name, column_name, type, options)
rename_column(table_name, column_name, new_column_name)
change_column(table_name, column_name, type, options)
remove_column(table_name, column_names)

add_index(table_name, column_names, options)
remove_index(table_name, :column => column_name)
remove_index(table_name, :name => index_name)
```

#### Reversible Migrations

```
class AddPartNumberToProducts < ActiveRecord::Migration
  def change
    add_column :products, :part_number, :string
  end
end

add_column
add_index
add_timestamps
create_table
remove_timestamps
rename_column
rename_index
rename_table
```

#### AR column types

```
:binary
:boolean
:date
:datetime
:decimal
:float
:integer
:primary_key
:string
:text
:time
:timestamp
```

Validation
----------

```
p = Person.new # => #<Person id: nil, name: nil>
p.errors # => {}
p.valid? # => false; triggers all validations and returns boolean
p.errors # => {:name=>["can't be blank"]}
p.save # => false
p.save! # => ActiveRecord::RecordInvalid: Validation failed: Name can't be blank

#triggers validation:
create
create!
save
save!
update
update_attributes
update_attributes!
```

### helpers

```
validates :password, :length => { :in => 6..20 } # or :minimum, :maximum, :is
validates :content, :length => {
    :minimum   => 300,
    :maximum   => 400,
    :tokenizer => lambda { |str| str.scan(/\w+/) },
    :too_short => "must have at least %{count} words",
    :too_long  => "must have at most %{count} words"
  }

validates :points, :numericality => true
validates :games_played, :numericality => { :only_integer => true }
:greater_than
:greater_than_or_equal_to
:equal_to
:less_than
:less_than_or_equal_to
:odd
:even

validates :terms_of_service, :acceptance => { :accept => 'yes' } # by default: :accept => "1"

has_many :books
validates_associated :books # do not validate associated in both related models

validates :email, :confirmation => true # checks "email_confirmation" field (only if is not nil)
validates :email_confirmation, :presence => true # must be added to require confirmation

validates :subdomain, :exclusion => { :in => %w(www us ca jp), :message => "Subdomain %{value} is reserved." }
validates :size, :inclusion => { :in => %w(small medium large), :message => "%{value} is not a valid size" }

validates :legacy_code, :format => { :with => /\A[a-zA-Z]+\z/, :message => "Only letters allowed" }

validates :email, :uniqueness => true
```

#### validates\_with

```
class Person < ActiveRecord::Base
  validates_with GoodnessValidator
end

class GoodnessValidator < ActiveModel::Validator
  def validate(record)
    if record.first_name == "Evil"
      record.errors[:base] << "This person is evil"
    end
  end
end
```

#### validates\_each

```
class Person < ActiveRecord::Base
  validates_each :name, :surname do |record, attr, value|
    record.errors.add(attr, 'must start with upper case') if value =~ /\A[a-z]/
  end
end
```

### validation common options

```
:allow_nil => true # ignored by presence validator
:allow_blank => true # ignored by presence validator
:message => "You got error here man!"
:on => :create # or :update, :save (:save is default)
```

### validation conditional

```
# Proc
validates :password, :confirmation => true, :unless => Proc.new { |a| a.password.blank? }

# symbol
validates :card_number, :presence => true, :if => :paid_with_card?
def paid_with_card?
  payment_type == "card"
end

#eval
validates :surname, :presence => true, :if => "name.nil?" # evaluates

#grouped
with_options :if => :is_admin? do |admin|
  admin.validates :password, :length => { :minimum => 10 }
  admin.validates :email, :presence => true
end
```

Callbacks
---------

#### create callbacks

```
before_validation
after_validation
before_save
around_save
before_create
around_create
after_create
after_save
```

#### update callbacks

```
before_validation
after_validation
before_save
around_save
before_update
around_update
after_update
after_save
```

#### destroy callbacks

```
before_destroy
around_destroy
after_destroy
```

#### find & initialize

```
after_find # from db
after_initialize # new or from db
```

#### callback triggers

```
create
create!
decrement!
destroy
destroy_all
increment!
save
save!
save(:validate => false)
toggle!
update
update_attribute
update_attributes
update_attributes!
valid?

# after_find triggers:
all
first
find
find_all_by_attribute
find_by_attribute
find_by_attribute!
last
```

#### callbacks conditional

```
# Proc, symbol, string like validation conditional=
```

### callback classes

```
#class method

class PictureFileCallbacks
  def self.after_destroy(picture_file)
    if File.exists?(picture_file.filepath)
      File.delete(picture_file.filepath)
    end
  end
end

class PictureFile < ActiveRecord::Base
  after_destroy PictureFileCallbacks
end

# instance method, wtf?
class PictureFileCallbacks
  def after_destroy(picture_file)
    if File.exists?(picture_file.filepath)
      File.delete(picture_file.filepath)
    end
  end
end

class PictureFile < ActiveRecord::Base
  after_destroy PictureFileCallbacks.new
end
```

Observers
---------

```
rails generate observer User

# app/models/user_observer.rb
class UserObserver < ActiveRecord::Observer
  def after_create(model)
    # code to send confirmation email...
  end
end

# config/application.rb
config.active_record.observers = :user_observer
```

#### sharing observers

```
class MailerObserver < ActiveRecord::Observer
  observe :registration, :user # Registration and User models are observed
  ...
end
```

Associations
------------

#### belongs\_to

```
belongs_to :customer

association(force_reload = false)
association=(associate)
build_association(attributes = {})
create_association(attributes = {})
```
