---
slug: "fastapi"
title: "FastAPI"
---


```python
from fastapi import FastAPI

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}
```
## Request


```python
from typing import Optional

from fastapi import Body, FastAPI
from pydantic import BaseModel

app = FastAPI()


class Item(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None


class User(BaseModel):
    username: str
    full_name: Optional[str] = None


@app.put("/items/{item_id}")
async def update_item(
    *,
    item_id: int,
    item: Item,
    user: User,
    importance: int = Body(..., gt=0),
    q: Optional[str] = None
):
    results = {"item_id": item_id, "item": item, "user": user, "importance": importance}
    if q:
        results.update({"q": q})
    return results
```


### Path Parameters

```python
# /items/foo
@app.get("/items/{item_id}")
async def read_item(item_id):
    return {"item_id": item_id}

```

#### types
```python
# /items/123
# /items/foo will raise validation error
@app.get("/items/{item_id}")
async def read_item(item_id: int):
    return {"item_id": item_id}
```

#### enum
```python
from enum import Enum

class ModelName(str, Enum):
    alexnet = "alexnet"
    resnet = "resnet"
    lenet = "lenet"

@app.get("/models/{model_name}")
async def get_model(model_name: ModelName):
    if model_name == ModelName.alexnet:
    # ...
```

#### path parameter containing path
```python
# /files/foo/bar/baz.png
@app.get("/files/{file_path:path}")
async def read_file(file_path: str):
    return {"file_path": file_path}

```

#### Path metadata

```python
from fastapi import FastAPI, Path, Query

@app.get("/items/{item_id}")
async def read_items(
    item_id: int = Path(..., title="The ID of the item to get"),
):
```

#### Path number validation

```python
item_id: int = Path(..., title="The ID of the item to get", gt=0, le=1000),

```



### Query Parameters

#### optional with default value

```python
# /items/?skip=0&limit=10
# /items/?skip=0
# /items/
@app.get("/items/")
async def read_item(skip: int = 0, limit: int = 10):
```

#### optional without default value

```python
from typing import Optional

@app.get("/items/{item_id}")
async def read_item(item_id: str, q: Optional[str] = None):
```

#### required

```python
# /items/foo-item?needy=sooooneedy
# /items/foo-item returns error
@app.get("/items/{item_id}")
async def read_user_item(item_id: str, needy: str):
```


#### type conversion

```python
# /items/foo?short=1
# /items/foo?short=True
# /items/foo?short=on
# /items/foo?short=yes

@app.get("/items/{item_id}")
async def read_item(item_id: str, q: Optional[str] = None, short: bool = False):
```

#### Query string validation

```python
from fastapi import FastAPI, Query

@app.get("/items/")
async def read_items(
    q: Optional[str] = Query(None, min_length=3, max_length=50, regex="^fixedquery$") 
):

# default value
q: str = Query('fixedquery', min_length=3)

# required
q: str = Query(..., min_length=3) 

# alias 
# /items/?item-query=foobaritems
q: Optional[str] = Query(None, alias="item-query")
```

#### Query metadata

```python

# meta data
q: Optional[str] = Query(
    None,
    title="Query string",
    description="Query string for the items to search in the database that have a good match",
    min_length=3,
)


```

#### Query multiple values 

```python
from typing import List

# http://localhost:8000/items/?q=foo&q=bar
q: Optional[List[str]] = Query(None)

# with defaults
# http://localhost:8000/items/
q: List[str] = Query(["foo", "bar"])

# without checking content of List
q: list = Query([])


```


### Request Body 


```python
from typing import Optional
from pydantic import BaseModel

class Item(BaseModel)
    name: str 
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None

# /items/ { "name": "foo", "price": 4.24 }
@app.post("/items/")
async def create_item(item: Item):
    return item

# embed (like in multiple)
# /items/ { "item": { "name": "foo", "price": 4.24 }}
@app.post("/items/")
async def create_item(item: Item = Body(..., embed=True)):
    return item
```


#### multiple body parameters


```python
class Item(BaseModel):
    name: str

class User(BaseModel):
    username: str

# /items/123 { "item": {"name": ...}, "user": {"username": ...}}
@app.put("/items/{item_id}")
async def update_item(item_id: int, item: Item, user: User):
    results = {"item_id": item_id, "item": item, "user": user}
    return results
```

#### singular body value

```python
# /items/123 { "item": {"name": ...}, "user": {"username": ...}, "importance": 10}
@app.put("/items/{item_id}")
async def update_item(
    item_id: int, item: Item, user: User, importance: int = Body(...)
):
```

#### dict of unknown keys

```python
from typing import Dict

@app.post("/index-weights/")
async def create_index_weights(weights: Dict[int, float]):
    return weights
```


#### Field validation & metadata

```python
from pydantic import BaseModel, Field

class Item(BaseModel):
    name: str
    description: Optional[str] = Field(
        None, title="The description of the item", max_length=300
    )
    price: float = Field(..., gt=0, description="The price must be greater than zero")
    tax: Optional[float] = None

@app.put("/items/{item_id}")
async def update_item(item_id: int, item: Item = Body(..., embed=True)):
    return item
```

### Request body nested models

```python
from pydantic import BaseModel, HttpUrl

class Image(BaseModel):
    url: HttpUrl # validation & documentation
    name: str


class Item(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None
    tags: Set[str] = set() # will convert to set of unique items
    images: Optional[List[Image]] = None

# {
#    "name": "Foo", "price": 42.0,
#    "tags": [ "rock", "metal", "bar" ],
#    "images": [
#        { "url": "http://example.com/baz.jpg", "name": "The Foo live" },
#        { "url": "http://example.com/dave.jpg", "name": "The Baz" }
#    ]
# }
@app.put("/items/{item_id}")
```

#### array of models


```python
class Image(BaseModel):
    url: HttpUrl
    name: str

@app.post("/images/multiple/")
async def create_multiple_images(images: List[Image]):
    return images
```

### parameters extra data types
[pydantic data types](https://pydantic-docs.helpmanual.io/usage/types/)

```python
from datetime import datetime, time, timedelta
from typing import Optional
from uuid import UUID

@app.put("/items/{item_id}")
async def read_items(
    item_id: UUID,
    start_datetime: Optional[datetime] = Body(None),
    end_datetime: Optional[datetime] = Body(None),
    repeat_at: Optional[time] = Body(None),
    process_after: Optional[timedelta] = Body(None),
):
```
### cookies

```python
from typing import Optional
from fastapi import Cookie, FastAPI

@app.get("/items/")
async def read_items(ads_id: Optional[str] = Cookie(None)):
    return {"ads_id": ads_id}

```

### headers

```python
from typing import Optional
from fastapi import FastAPI, Header

@app.get("/items/")
async def read_items(user_agent: Optional[str] = Header(None)):
    return {"User-Agent": user_agent}
```

#### duplicate

```python
@app.get("/items/")
async def read_items(x_token: Optional[List[str]] = Header(None)):
    return {"X-Token values": x_token}
```


## Response

### Response model

* Convert the output data to its type declaration.
* Validate the data.
* Add a JSON Schema for the response, in the OpenAPI path operation
* Will limit the output data to that of the model

```python
class Item(BaseModel):
    name: str
    price: float

@app.post("/items/", response_model=Item)
async def create_item(item: Item):
    return item
```

#### output model


```python
class UserIn(BaseModel):
    username: str
    password: str
    email: EmailStr

class UserOut(BaseModel): # missing password in output
    username: str
    email: EmailStr

@app.post("/user/", response_model=UserOut)
async def create_user(user: UserIn):
    return user
```

### include exclude 
[exporting models](https://pydantic-docs.helpmanual.io/usage/exporting_models/#modeldict)

#### response\_model\_exclude_unset

Does not include default values if not set

```python
class Item(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: float = 10.5

items = {
    "foo": {"name": "Foo", "price": 50.2},
    "bar": {"name": "Bar", "description": "The bartenders", "price": 62, "tax": 20.2},
}

# /item/foo => { "name": "Foo", "price": 50.2 }
@app.get("/items/{item_id}", response_model=Item, response_model_exclude_unset=True)
async def read_item(item_id: str):
    return items[item_id]

```

#### response_model_include response_model_exclude
It is still recommended to use multiple classes, instead of these parameters.

```python
@app.get(
    "/items/{item_id}/name",
    response_model=Item,
    response_model_include={"name", "description"},
)
async def read_item_name(item_id: str):
    return items[item_id]
```

#### Extra models


```python
from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel, EmailStr

app = FastAPI()

class UserBase(BaseModel):
    username: str
    email: EmailStr
    full_name: Optional[str] = None

class UserIn(UserBase):
    password: str

class UserOut(UserBase):
    pass

class UserInDB(UserBase):
    hashed_password: str

def fake_password_hasher(raw_password: str):
    return "supersecret" + raw_password

def fake_save_user(user_in: UserIn):
    hashed_password = fake_password_hasher(user_in.password)
    user_in_db = UserInDB(**user_in.dict(), hashed_password=hashed_password)
    print("User saved! ..not really")
    return user_in_db

@app.post("/user/", response_model=UserOut)
async def create_user(user_in: UserIn):
    user_saved = fake_save_user(user_in)
    return user_saved
```

#### Union (anyOf)

```python
from typing import Union

class CarItem(BaseItem):
    type = "car"


class PlaneItem(BaseItem):
    type = "plane"
    size: int

@app.get("/items/{item_id}", response_model=Union[PlaneItem, CarItem])
async def read_item(item_id: str):
    return items[item_id]
```

#### List of models

```python
from typing import List

class Item(BaseModel):
    name: str
    description: str

@app.get("/items/", response_model=List[Item])
async def read_items():
    return items
```

#### Dict 

```python
from typing import Dict

@app.get("/keyword-weights/", response_model=Dict[str, float])
async def read_keyword_weights():
    return {"foo": 2.3, "bar": 3.4}
```



