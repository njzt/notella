---
slug: "nginx"
title: "nginx"
---
start stop reload
-----------------

### signals

```
nginx # starts
nginx -s stop # fast shutdown
nginx -s quit # graceful shotdown
nginx -s reload # reloads configuration, starts new workers if success, then exits old ones
nginx -s reopen # reopen log files
```

### processes

```
ps -ax | grep nginx
kill -s QUIT 1628
```

configuration
-------------

### context

```
user nobody; # a simple directive in the 'main' context

events { # block directive
  # configuration of connection processing
}

http {
  # Configuration specific to HTTP and affecting all virtual servers  

  server {
    # configuration of HTTP virtual server 1       
    location /one {
      # configuration for processing URIs starting with '/one'
    }
    location /two {
      # configuration for processing URIs starting with '/two'
    }
  } 

  server {
    # configuration of HTTP virtual server 2
  }
}

stream {
  # Configuration specific to TCP/UDP and affecting all virtual servers
  server {
    # configuration of TCP virtual server 1 
  }
}
```

### static files

```
server { # block directive
  location / { # block directive
    root /data/www; # simple directive
  }
  location /images/ {
    root /data;
  }
}
```
