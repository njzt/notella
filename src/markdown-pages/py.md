---
slug: "py"
title: "py"
---
virtualenv
----------

```
$ cd project_folder
$ virtualenv venv # ‘venv’ is the general convention used globally
$ virtualenv -p python3 venv
```

re
--

```
import re

prog = re.compile(pattern) # reusing compiled regex may increase performance
result = prog.match(string)
# or equivalent
result = re.match(pattern, string)
```

### activate

```
# To begin using the virtual environment
# change your $PATH so its first entry is the virtualenv’s bin/ directory
$ source venv/bin/activate
$ pip install requests
$ deactivate
```

### freeze

```
# run when virtualenv activated:
$ pip freeze > requirements.txt
$ pip list

# later
pip install -r requirements.txt
```

### autoenv

challenges
----------

```
''.join(chr(96 + (ord(i) - 96 + 2 )% 26 ) if ord(i)>90 else i for i in a)
```

django
------

### project & app

```
$ django-admin startproject myproject .
$ python manage.py startapp myapp
```

### environ

```
$ pip install django-environ

# myproject/.env
# myproject/.env.example
DEBUG=on                                                                                                                                                                        
SECRET_KEY=my_secret_key                                                                                                                                                        
DATABASE_URL=mysql://user:password@localhost/dbname

[https://django-environ.readthedocs.io/en/latest/]
```

### migration

```
# when model changes are ready
$ python manage.py makemigrations blog # make migration
$ python manage.py sqlmigrate blog 0002 # preview
$ python manage.py migrate # run
```
