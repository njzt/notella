---
slug: "git"
title: "git"
---
config
------

```
git config --global user.name "Your Name"
git config --global user.email "your_email@whatever.com"
git config --global core.editor vim
git config --global merge.tool vimdiff

git config --list # lists all config entries
git config user.name # => Your Name

git config --global color.ui true
```

### .gitconfig

#### aliases

```
[alias]
  co = checkout
  ci = commit
  st = status
  br = branch
  hist = log --pretty=format:\"%h %ad | %s%d [%an]\" --graph --date=short
  type = cat-file -t
  dump = cat-file -p
```

log
---

```
git log --graph --decorate --oneline  
git log --all --pretty=format:"%h %cd %s (%an)" --since='7 days ago' # => 28e4b5c Wed Nov 21 23:52:21 2012 +0100 first commit (drugimuchem)
git log --pretty=format:"%h %ad | %s%d [%an]" --graph --date=short   # => * 28e4b5c 2012-11-21 | first commit [drugimuchem]

git log --oneline -p # show code changes
git log --oneline --stat # show number of lines changed

git log --since=1.day.ago
git log --until=1.minute.ago --since=2000-01-01
```

diff
----

```
# commits 
git diff HEAD^^
git diff HEAD~5 # five commits ago
git diff 7812daw8..dwadwa88d

#branches
git diff master bird

#time based
git diff --since=1.week.ago
```

blame
-----

```
git blame index.html --date short
```

undoing changes
---------------

#### clear Working Directory changes

```
git checkout hello.rb
```

#### unstage staged changes (still in Working Directory)

```
git reset HEAD hello.rb
```

#### clear staged and Working Directory changes

```
git checkout HEAD hello.rb
```

#### committed changes

```
git revert HEAD # new commit to revert last committed change
git reset --hard tag7 # master will point to tag7, --hard for drop all local changes
```

#### revert to specified commit

```
git reset 56e05fced # reset the index to the desired tree
git reset --soft HEAD@{1} # move the branch pointer back to the previous HEAD
git commit -m "Revert to 56e05fced"
git reset --hard # Update working copy to reflect the new commit
```

remotes
-------

```
git branch -a # show branches from all remotes
git remote prune origin # removes local "remote/<name>" branches which are already removed on remote
git push origin :branch_deprecated # removes branch from remote
git pull 
# equivalent to
git fetch
git merge origin/master
```

tags
----

```
git tag # list all tags
git checkout v0.0.1 # check out code at commit 
git tag -a v0.0.3 -m 'version 0.0.3 is here!' # add new tag

git push --tags # push tags to remotes
```

stash
-----

### create

```
git stash
git stash -p # interactive, partial stash
git stash save 'my useful console logs'  # named partial

git stash branch add-style stash@{1} # create new branch with stash
```

### show

```
git stash list
git stash show stash@{2} -p # show details of specific stash
```

### use

```
git stash pop # use (and remove from stash list) the most recent stash
git stash apply # use (and leave on stash list) the most recent stash

git stash pop stash@{2} # pops specific stash
```

bashrc utilities
----------------

```
alias gs='git status '
alias ga='git add '
alias gb='git branch '
alias gc='git commit'
alias gd='git diff'
alias go='git checkout '
alias gk='gitk --all&'
alias gx='gitx --all'
```

not tracked
-----------

```
.gitignore
```

### excluding only on local machine

```
.git/info/exclude # file
```

### untracking

```
git rm --cached file # keep file, remove from git
# then add it to gitignore
```
