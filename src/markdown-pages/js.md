---
slug: "js"
title: "js"
---
generator
---------

### yield

```
function* multiplier(v) { 
  var a = 1; 
  while(true) { yield a = a*v }
}

m = multiplier(2);
m.next(); // => { value: 2, done: false }
m.next(); // => { value: 4, done: false }
m.next(); // => { value: 8, done: false }
```

### yield\*

```
function* genString() {
  yield* 'yo!';
}
g = genString();
Array.from(g) // => [ 'y', 'o', '!' ]
Array.from(g) // => []
g.next() // => { value: undefined, done: true }
```

### Symbol.iterator

```
var countDown = { 
  [Symbol.iterator]:() => ({ 
    items: [3,2,1,'go!'], 
    next: function() { 
      return {
        done: this.items.length === 0, 
        value: this.items.shift() 
      }
    } 
  }) 
};

Array.from(countDown) // => [3,2,1,'go!']
Array.from(countDown) // => [3,2,1,'go!']
```
