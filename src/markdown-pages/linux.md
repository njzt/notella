---
slug: "linux"
title: "linux"
---
tmux
----

```
tmux ls
tmux kill-session -t 12
tmux a -t 12 # attach
```

terminal tools
--------------

#### how to rename many files at once

```
rename -n 's/$/.txt/' abc* # -n is test mode, no changes made
rename -v 's/$/.txt/' abc* # changes made with verbose
```

issues
------

#### dns cache

```
sudo apt-get install nscd
sudo /etc/init.d/nscd restart
```
