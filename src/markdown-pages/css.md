---
slug: "css"
title: "css"
---
#### specifity of selectors

```
#sidebar p:first-line — 0, 1, 0, 2
#sidebar h2 — 0, 1, 0, 1
h2.title — 0, 0, 1, 1
h2 + p — 0, 0, 0, 2
```

#### box model

```
content area
padding area
border area
margin area
```

#### positioning

```
static // default

// positioned (may use top/left/right/bottom):
relative
absolute
fixed
```

#### display

```
box model
+ block
+ inline-block
- inline

new line before and after element
+ block
- inline-block
- inline

stretches to width of content
- block (100%)
+ inline-block
+ inline
```

#### content

```
article p:last-child:after {
  content: '\2744'; /*snowflake at the end*/
}

blockquote:before { /* decorate quote with image, no extra element */
  content: '';
  backgroun: url(quote.png);
  position: absolute;
}
```
