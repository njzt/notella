---
slug: "sass"
title: "Sass"
---
Basic
-----

### comments

```
// missing in compiled output
/* saved in output */
```

```
/* saved in output */
```

### import

```
// application.scss
@import 'buttons'; // _buttons.sass, buttons.sass, _buttons.scss or buttons.scss
```

Nesting
-------

### nesting selectors

```
.content {
  margin: 10px;
  h2 {
    font-size: 130%;
  }
}
```

```
.content {
  margin: 10px;
}
.content h2 {
  font-size: 130%;
}
```

### nesting properties

```
.btn {
  text: {
    decoration: underline;
    transform: lowercase;
  }
}
```

```
.btn {
  text-decoration: underline;
  text-transform: lowercase;
  }
}
```

### & (parent selector)

```
a.blink {
  color: black;
  &:hover {
    color: blue;
  }
  .sidebar & {
    font-size: 90%;
  }
}
```

```
a.blink {
  color: black;
}
a:hover {
  color: blue;
}
.sidebar a.blink {
  font-size: 90%;
}
```

Variables
---------

```
$title 'My Blog';
h2:before {
  content: $title;
}
```

```
h2:before {
  content: 'My Blog';
}
```

### !default

```
$title 'About me';
$title 'My Blog' !default; // do not overwrite $title if exists
h2:before {
  content: $title;
}
```

```
h2:before {
  content: 'About me';
}
```

### Types

```
// boolean
$rounded: true;

// numbers
$line-height: 1.5; // may be set without unit
$rounded: 4px;

// colors
$base: purple;
$shadow: #333;

// strings
$header: 'Helvetica Neue'; // with or without quotes
$callout: Arial;

// lists
$authors: nick, dan, aimee, draw; // comma or spaces separated
$margin: 40px 0 2px 10px;

//null
$shadow: null;
```

### interpolation

```
// interpolates variable into selectors, property names and strings
$side: top;
sup {
  position: relative;
  #{$side}: -0.5em;
}
callout-#{$side} {
  background: #777;
}
```

### Lists

```
$authors: nick aimee dave
length($authors) // => 3
append($authors, tim) // => nick aimee dave tim

$authors2: lilly sue
join($authors, $authors2) // => nick aimee dave tim lilly sue
index($authors2, sue) // => 2, first index is 1 not 0
nth($authors2, 2) // => sue
```

#### zip

```
$authors: lilly sue
$colors: blue violet
$authors-styled: zip($authors, $colors) // list of pairs

@each $a in $authors-styled
  .author-#{nth($a,1)}
    background: nth($a, 2)
```

Mixins
------

```
@mixin button {
  border: 1px solid #ccc;
  text-transform: uppercase;
}
.btn-a {
  @include button;
  background: #f77;
}
```

```
.btn-a {
  border: 1px solid #ccc;
  text-transform: uppercase;
  background: #f77;
}
```

### arguments

```
@mixin button($radius, $color) {
  border-radius: $radius;
  color: $color;
}
.btn-a {
  @include button(10px, #333);
  background: #ddf;
}
```

#### default value

```
@mixin box-sizing($x: content-box) {
  -webkit-box-sizing: $x;
  -moz-box-sizing: $x;
  box-sizing: $x;
}
```

#### named arguments

```
@mixin button($radius, $color) {
  border-radius: $radius;
  color: $color;
}
.btn-a {
  @include button($radius: 10px, $color: #333);
  background: #ddf;
}
```

#### variable argument (vararg)

```
@mixin transition($val...) { // more comma separated values comes into $val
  -webkit-transition: $val;
  -moz-transition: $val;
  transition: $val;
}
.btn-a {
  @include transition(color 0.3s ease-in, background 0.5s ease-out);
}
```

#### vararg in reverse

```
@mixin button($radius, $color) {
  border-radius: $radius;
  color: $color;
}
$properties: 4px, #000;
.btn-a {
  @include button($properties...); // be splitted into arguments by mixin
  background: #ddf;
}
```

Extend
------

```
.btn-a {
  border: 1px solid #ccc;
  text-transform: uppercase;
  background: #f77;
}
.btn-b {
  @extend .btn-a;
  background: #99f;
}
```

```
.btn-a, .btn-b {
  border: 1px solid #ccc;
  text-transform: uppercase;
  background: #f77;
}
.btn-b {
  background: #99f;
}
```

### extend & nesting

```
.content {
  padding: 20px;
  h2 {
    font-size: 3em;
  }
}
.callout {
  @extend .content;
  background: #333;
}
```

```
.content, .callout {
  padding: 20px;
}
.content h2, .callout h2 {
  font-size: 3em;
}
.callout {
  background: #333;
}
```

### Placeholder selectors

```
// instead of mixins without arguments
%btn {
  background: #699;
  border: 1px solid #333;
}
.btn-a {
  @extend %btn;
}
.btn-b {
  @extend %btn;
  color: #111;
}
```

Directives
----------

```
@function fluidize($target, $context) {
  @return ($target/$contect) * 100%;
}

.sidebar {
  width: fluidize(350px, 1000px);
}
```

```
.sidebar {
  width: 35%;
}
```

### if

```
$theme: dark;
.header {
  @if $theme == dark {
    background: #000;
  } @else if $theme == pinf {
    background: pink;
  } @else {
    background: #fff;
  }
}

// single line if
background: if($theme == dark, #000, #fff);
```

### each

```
$authors: nick aimee dave;
@each $author in $authors {
  .author-#{$author} {
    background: url(author-#{$author}.png);
  }
}
```

### for & while

#### for

```
item {
  position: absolute;
  @for $item from 1 through 3 {
    &.item-#{$item} {
      top: $item*30px;
    }
  }
}
```

```
.item {
  position: absolute;
}
.item.item-1 {
  position: absolute;
  top: 30px;
}
// etc.
```

#### while

```
$i: 2;
.item {
  position: absolute;
  @while $i < 8 {
    &.item-#{$i} {
      top: $i*30px;
      $i: $i+2;
    }
  }
}
```

Math & Color
------------

```
+ - * / %
```

### triggering divisions

```
$size / 10 // variable involved
(100px / 20) // parenthesis
20px * 5 / 2 // part of bigger operation
```

### different units

```
4px + 2pt
4px + 2em // error; incompatible units
```

### utilities

```
round($number)
ceil($number)
floot($number)
abs($number)

min($list)
max($list)

percentage($number)
```

### Colors

```
$color: #333333;
.alpha {
  background: rgba($color, 0.8);
}
```

```
.alpha {
  background: rgba(51,51,51,0.8);
}
```

#### Color utility functions

```
$color: #444;
$color2: blue;

color: lighten($color, 20%);
background: darken($color, 20%);

color: saturate($color, 20%);
background: desaturate($color, 20%);

color: mix($color, $color2);
background: mix($color, $color2, 30%); // percent of first color

color: grayscale($color2);
background: invert($color);
border-color: complement($color); // opposing

// and many more
```

#### scale\_color

```
//changes one or more colors aspects relatively to the start value
$red
$green
$blue
$saturation
$alpha
$lightness

color: scale_color(#eee, $lightness: 7%)
```

Media queries
-------------

```
.sidebar {
  border: 1px solid #ccc;
  @media (min-width: 700px) { // if viewport is at least 700px width
    width: 30%;
    float: right;
  }
}
```

```
.sidebar {
  border: 1px solid #ccc;
}
@media (min-width: 700px) { 
  .sidebar {
    width: 30%;
    float: right;
  }
}
```
