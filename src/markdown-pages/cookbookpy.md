---
slug: "cookbookpy"
title: "cookbook.py"
---
```
https://www.oreilly.com/library/view/python-cookbook-3rd/9781449357337/ch01.html
https://books.google.pl/books?id=f7wGeA71_eUC&printsec=frontcover&hl=pl#v=onepage&q&f=false
```

Data Structures and Algorithms
------------------------------

### unpacking from iterables

```
>>> data = [ 'ACME', 50, 91.1, (2012, 12, 21) ]
>>> _, shares, price, _ = data
```

#### arbitrary length

```
def drop_first_last(grades):
  first, *middle, last = grades
  return avg(middle)

>>> record = ('Dave', 'dave@example.com', '773-555-1212', '847-555-1212')
>>> name, email, *phone_numbers = user_record

>>> record = ('ACME', 50, 123.45, (12, 18, 2012))
>>> name, *_, (*_, year) = record
```

### keeping last N items #deque

```
# Adding or popping items from either end of a queue has O(1) complexity. This is unlike
# a list where inserting or removing items from the front of the list is O(N).
from collections import deque 

def search(lines, pattern, history=5):
  previous_lines = deque(maxlen=history)
  for line in lines:
    if pattern in line:
      yield line, previous_lines
    previous_lines.append(line)

# Example use on a file
if __name__ == '__main__':
  with open('somefile.txt') as f:
    for line, prevlines in search(f, 'python', 5):
      for pline in prevlines:
        print(pline, end='')
      print(line, end='')
      print('-'*20)
```

### finding the largest or smallest N items #heapq

```
>>> import heapq
>>> nums = [1, 8, 2, 23, 7, -4, 18, 23, 42, 37, 2]
>>> print(heapq.nlargest(3, nums))
[42, 37, 23]
>>> print(heapq.nsmallest(3, nums))
[-4, 1, 2]
```

#### key

```
>>> oceans
[{'name': 'Pacific', 'size': 168723000}, {'name': 'Atlantic', 'size': 85133000}, {'name': 'Indian', 'size': 70560000}, {'name': 'Southern', 'size': 21960000}, {'name': 'Arctic', 'size': 15558000}]
>>> heapq.nsmallest(2, oceans, key=lambda s: s['size'])
[{'name': 'Arctic', 'size': 15558000}, {'name': 'Southern', 'size': 21960000}]
>>> heapq.nlargest(1, oceans, key=lambda s: s['size'])
[{'name': 'Pacific', 'size': 168723000}]
```

### priority queue #heapq #tuple

```
>>> import heapq
>>> class PriorityQueue:                                                                                                                                                                                           
...   def __init__(self):
...     self._queue = []
...     self._index = 0
...   def push(self, item, priority):                                                                                                                                                                              
...     heapq.heappush(self._queue, (-priority, self._index, item)) # make tuple to sort by priority desc, insertion order asc, see "tuples comparison" below                                                                                                                                           
...     self._index += 1                                                                                                                                                                                           
...   def pop(self):
...     return heapq.heappop(self._queue)[-1] # return item from tuple
... 
>>> class Item:
...   def __init__(self, name):
...     self.name = name
...   def __repr__(self):
...     return 'Item({!r})'.format(self.name)
... 
>>> q = PriorityQueue()
>>> q.push(Item('foo'), 1)
>>> q.push(Item('bar'), 5)
>>> q.push(Item('spam'), 4)
>>> q.push(Item('grok'), 1)
>>> q.pop()
Item('bar')
>>> q.pop()
Item('spam')
>>> q.pop()
Item('foo')
>>> q.pop()
Item('grok')
```

#### tuples comparison

```
# (priority, item) tuples can be compared as long as the priorities are different
>>> a = (1, 0, Item('foo'))
>>> b = (1, 1, Item('bar'))                                                                                                                                                                                        
>>> c = (2, 0, Item('spam'))
>>> d = (2, 0, Item('ups'))
>>> b > a
True
>>> c > b
True
>>> d > c
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: '>' not supported between instances of 'Item' and 'Item'
```

### multidict #defaultdict #setdefault #dict

```
>>> from collections import defaultdict
>>> d = defaultdict(list)
>>> d['a'].append('foo')
>>> d['a'].append('bar')
>>> d['b'].append('B1')
>>> d
defaultdict(<class 'list'>, {'a': ['foo', 'bar'], 'b': ['B1']})
>>> len(d['a'])
2
# automatically create dictionary entries for keys accessed later on (even if they aren’t currently found in the dictionary)
>>> len(d['z'])
0
>>> d
defaultdict(<class 'list'>, {'a': ['foo', 'bar'], 'b': ['B1'], 'z': []})
```

#### less clean code with setdefault

```
>>> d = {}
>>> d.setdefault('a',[]).append('foo')
>>> d.setdefault('a',[]).append('bar')
>>> d
{'a': ['foo', 'bar']}
```

### keeping dictionary in order #ordereddict

```
# !Be aware - the size of ordereddict is more than twice as large as a normal dictionary
>>> from collections import OrderedDict
>>> d = OrderedDict()
>>> d['foo']=1
>>> d['bar']=2
>>> d['spam']=3
# preserves the original insertion order when iterating
>>> for key in d: # outputs foo, bar,spam
...   print key
```

### find most frequently occurring items #counter

```
>>> words = [ 'look', 'into', 'my', 'eyes', 'look', 'into', 'my', 'eyes', 'the', 'eyes', 'the', 'eyes', 'the', 'eyes', 'not', 'around']
>>> from collections import Counter
>>> word_counts = Counter(words)
>>> word_counts.most_common(3)
[('eyes', 5), ('the', 3), ('into', 2)]
# add more words
>>> morewords = ['why','are','you','not','looking','in','my','ey']
>>> word_counts.update(morewords)
```

#### operations

```
>>> Counter({'foo':2, 'bar': 3}) + Counter({'foo':4, 'baz': 1})
Counter({'foo': 6, 'bar': 3, 'baz': 1})
>>> Counter({'foo':2, 'bar': 3}) - Counter({'foo':4, 'baz': 1})
Counter({'bar': 3})
```

### commonalities in dictionaries

```
>>> a = {'x':1, 'y':2, 'z':3}
>>> b = {'w':10, 'x':10, 'y':2}

>>> type(a.keys())
<class 'dict_keys'>
>>> a.keys() & b.keys()
{'y', 'x'}
>>> a.keys() - b.keys()
{'z'}

>>> type(a.items())
<class 'dict_items'>
>>> a.items() & b.items()
{('y', 2)}

# new dictionary with filtered out keys
c = {key:a[key] for key in a.keys() - {'z', 'w'}} # c is {'x': 1, 'y': 2}

# in python 2 need set support
>>> set(a.keys()) & set(b.keys())                                                                                                                                                                                  
set(['y', 'x'])
```

### naming a slice #slice

```
>>> items = [0, 1, 2, 3, 4, 5, 6]
>>> a = slice(2, 4)
>>> items[2:4]
[2, 3]
>>> items[a]
[2, 3]
>>> items[a] = [10,11]
>>> items
[0, 1, 10, 11, 4, 5, 6]
>>> del items[a]
>>> items
[0, 1, 4, 5, 6]
```

### sorting list of dictionaries #sorted #itemgetter

```
>>> rows = [
...     {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003},
...     {'fname': 'David', 'lname': 'Beazley', 'uid': 1002},
...     {'fname': 'John', 'lname': 'Cleese', 'uid': 1001},
...     {'fname': 'Big', 'lname': 'Jones', 'uid': 1004}
... ]
# sorted 'key' must be callable that takes a single item from rows and returns a value that will be used as the basis for sorting

# lambda
>>> sorted(rows, key=lambda r: r['lname']) # or modify rows with rows.sort(key=lambda r: r['lname'])
>>> sorted(rows, key=lambda r: (r['lname'], r['fname']))

# a bit faster with itemgetter
>>> from operator import itemgetter
>>> rows_by_lname = sorted(rows, key=itemgetter('lname')) 
>>> rows_by_lfname = sorted(rows, key=itemgetter('lname', 'fname'))
```

#### min, max

```
>>> min(rows, key=itemgetter('uid'))
{'fname': 'John', 'lname': 'Cleese', 'uid': 1001}
>>> max(rows, key=itemgetter('uid'))
{'fname': 'Big', 'lname': 'Jones', 'uid': 1004}
```

### sorting objects #attrgetter

```
>>> class User:
...   def __init__(self, user_id):
...     self.user_id = user_id
...   def __repr__(self):
...     return 'User({})'.format(self.user_id)
... 
>>> users = [User(23), User(3), User(99)]
# with lambda
>>> sorted(users, key=lambda u: u.user_id)
[User(3), User(23), User(99)]
# a bit faster with attrgetter
>>> from operator import attrgetter
>>> sorted(users, key=attrgetter('user_id'))
[User(3), User(23), User(99)]
```

### groupby #groupby

```
>>> countries = [
...   {'country':'Poland', 'continent':'Europe'},
...   {'country':'Chile', 'continent':'South America'},
...   {'country':'Italy', 'continent':'Europe'},
...   {'country':'Argentina', 'continent':'South America'},
... ]
>>> from operator import itemgetter
>>> from itertools import groupby
# groupby() only examines consecutive items, must sort first
>>> countries.sort(key=itemgetter('country'))
>>> for continent, items in groupby(countries, key=itemgetter('continent')):
...   print(continent)
...   for i in items:
...     print('    ', i)
... 
South America
     {'country': 'Argentina', 'continent': 'South America'}
     {'country': 'Chile', 'continent': 'South America'}
Europe
     {'country': 'Italy', 'continent': 'Europe'}
     {'country': 'Poland', 'continent': 'Europe'}

# alternative is use defaultdict, no need to sort but more memory is used
from collections import defaultdict
by_continent = defaultdict(list)
for country in countries:
    by_continent[row['continent']].append(country)
```

### filtering #comprehension #filter #compress

```
>>> mylist = [1, 4, -5, 10, -7, 2, 3, -1] # list comprehension
>>> [n for n in mylist if n > 0]

>>> [n if n >= 0 else 0 for n in mylist] # with modification
[1, 4, 0, 10, 0, 2, 3, 0]

[1, 4, 10, 2, 3]
# less memory used when use generator instead of list comprehension
>>> pos = (n for n in mylist if n > 0)
>>> pos
<generator object <genexpr> at 0x7fe9809942b0>
>>> for x in pos:
...   print(x)
```

#### filter

```
# useful if filtering process involves exception handling or some other complicated detail
>>> mylist = [1, 4, -5, 10, '-', 2, 'N/A', -1]
>>> def is_int(val):
...   try:
...     x = int(val)
...     return True
...   except ValueError:
...     return False
... 
>>> list(filter(is_int, mylist)) # filter creates iterator
[1, 4, -5, 10, 2, -1]
```
