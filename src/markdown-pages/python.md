---
slug: "python"
title: "python"
---
variables
---------

#### id

```
>>> x = 42
>>> y = x
>>> id(x), id(y)
(10936800, 10936800)
>>> y = 78
>>> id(x), id(y)
(10936800, 10937952)
```

#### is

```
>>> a = 'hello'
>>> b = 'hello'
>>> id(a), id(b)
(140183130582632, 140183130582632)
>>> a is b
True
>>> a = 'hello!'
>>> b = 'hello!'
>>> id(a), id(b)
(140183082040096, 140183084963968)
>>> a is b
False
```

#### type + dir

```
>>> type('dupa')
<class 'str'>
>>> dir('dupa')
['__add__', '__class__', '__contains__', '__delattr__', '__dir__', '__doc__',..., 'title', 'translate', 'upper', 'zfill']
```

types
-----

### numbers

#### integer

```
>>> 42
42
>>> bin(42)
'0b101010'
>>> oct(42)
'0o52'
>>> hex(42)
'0x2a'
```

#### long integer

```
# integer can be of unlimited size
# Python 2 has int and long, Python 3 only int
```

#### floating-point numbers

```
>>> 42.11
42.11
```

#### complex numbers

```
>>> x = 3 + 4j
```

#### arithmetic operators

```
# division always returns a floating point number
17 / 3 # => 5.666666666666667
# floor division discards the fractional part
17 // 3 # => 5
17 % 3 # => 2
5 * 3 + 2 # => 17
2 ** 7 # => 128
```

### sequence types

```
Python provides for six sequence (or sequential) data types:
 strings
 byte sequences
 byte arrays
 lists
 tuples
 range objects
```

#### common sequence operations

```
x in s     # True if an item of s is equal to x, else False	(1)
x not in s # False if an item of s is equal to x, else True	(1)
s + t      # the concatenation of s and t	(6)(7)
s * n or n * s # equivalent to adding s to itself n times	(2)(7)
s[i]     # ith item of s, origin 0	(3)
s[i:j]   # slice of s from i to j	(3)(4)
s[i:j:k] # slice of s from i to j with step k	(3)(5)
len(s)   # length of s	 
min(s)   # smallest item of s	 
max(s)   # largest item of s	 
s.index(x[, i[, j]]) # index of the first occurrence of x in s (at or after index i and before index j)	(8)
s.count(x)           # total number of occurrences of x in s	 
```

#### mutable sequence types

```
s[i] = x      # item i of s is replaced by x	 
s[i:j] = t    # slice of s from i to j is replaced by the contents of the iterable t	 
del s[i:j]    # same as s[i:j] = []	 
s[i:j:k] = t  # the elements of s[i:j:k] are replaced by those of t	(1)
del s[i:j:k]  # removes the elements of s[i:j:k] from the list	 
s.append(x)   # appends x to the end of the sequence (same as s[len(s):len(s)] = [x])	 
s.clear()     # removes all items from s (same as del s[:])	(5)
s.copy()      # creates a shallow copy of s (same as s[:])	(5)
s.extend(t) or s += t # extends s with the contents of t (for the most part the same as s[len(s):len(s)] = t)	 
s *= n	        # updates s with its contents repeated n times	(6)
s.insert(i, x)	# inserts x into s at the index given by i (same as s[i:i] = [x])	 
s.pop([i])	# retrieves the item at i and also removes it from s	(2)
s.remove(x)	# remove the first item from s where s[i] == x	(3)
s.reverse()	# reverses the items of s in place
```

### strings

```
'"Isn\'t," they said.' # => '"Isn\'t," they said.'
print('"Isn\'t," they said.') # => "Isn\'t," they said.
print(r'C:\some\name') # => C:\some\name; ignore new name with the r before the quote
# * and + 
3 * 'un' + 'ium' # => 'unununium'
```

#### multiple lines

```
my_very_big_string = (
    "For a long time I used to go to bed early. Sometimes, "
    "when I had put out my candle, my eyes would close so quickly "
    "that I had not even time to say “I’m going to sleep.”"
)

# triple quotes """ or '''
# \ at the end of lines excludes new lines
print("""\
Usage: thingy [OPTIONS]
     -h                        Display this usage message
     -H hostname               Hostname to connect to
""")
```

#### concatenation

```
'Py'+'thon' # => 'Python'
# Two or more string literals next to each other are automatically concatenated.
'Py' 'thon' # => 'Python'
>>> text = ('Put several strings within parentheses '
...         'to have them joined together.')
```

#### repetition

```
>>> "bla" * 3
'blablabla'
>>> len('Python')
6
```

#### indexing & slicing

```
word = 'Python'
word[0] # => 'P'
word[-1] # => 'n'
word[0:2] # => 'Py'
word[:2] # => 'Py'
word[4:] # => 'on'
word[-2:] # => 'on'
# too large index throws error but works fine in slicing
word[42] # => IndexError: string index out of range
word[4:42] # => 'on'
# immutable
word[0] = 'J' # TypeError: 'str' object does not support item assignment
```

#### methods

```
https://docs.python.org/3.6/library/stdtypes.html#string-methods
```

### lists

```
squares = [1, 4, 9, 16, 25]
squares[-3:] # => [9, 16, 25]; new list
squares[:] # => [1, 4, 9, 16, 25]; shallow copy
squares + [36, 49] => [1, 4, 9, 16, 25, 36, 49]
```

#### constructors

```
a = [] # square brackets
[x for x in iterable] # list comprehension
[x for x in 'abc'] # => ['a','b','c']
list(iterable) # type constructor
list('abc') # => ['a','b','c']
four_nones = [None] * 4 # => [None, None, None, None]
four_lists = [[] for __ in range(4)]
```

#### lists are a mutable type

```
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
letters[2:5] = ['C', 'D', 'E'] # letters is ['a', 'b', 'C', 'D', 'E', 'f', 'g']
letters[2:5] = [] # letters is ['a', 'b', 'f', 'g']
letters[:] = [] # letters is []
```

#### methods

```
letters = ['a','b','c']
len(letters) # => 3

# add item to end of list
letters.append('d') # letters is ['a','b','c','d']
# same as
letters[len(letters):] = 'd'

# extend with iterable
letters.extend(['e','f']) # letters is ['a','b','c','d','e','f'] 
# same as
letters[len(letters):] = ['e','f']

# remove first item with given value
letters.remove('d') # letters is ['a','b','c','e','f']
letters.remove('xxx') # ValueError: list.remove(x): x not in list

# insert an item at a given position
letters.insert(3, 'd') # letters is ['a','b','c','d','e','f']

# remove item at a given position (or last) and returns it
letters.pop() # => 'f'; letters is ['a','b','c','d','e']
letters.pop(3) # => 'd'; letters is ['a','b','c','e']

# returns index of item
letters.index('c') # => 2
# list.index(x[, start[, end]])
letters.index('c', 3) # ValueError: 'c' is not in list

# shallow copy
letters.copy() # => ['a','b','c','e']

# reverse list in place
letters.reverse() # letters is ['e', 'c', 'b', 'a']

# sort list in place
letters.sort() # letters is ['a','b','c','e']

# count elements with given value
letters.count('b') # => 1

# removes all elements
letters.clear()
# same as
letters[:]=[]
```

#### lists as stacks

```
>>> stack = [5,6,7]
>>> stack.append(8)
>>> stack
[5, 6, 7, 8]
>>> stack.pop()
8
>>> stack
[5, 6, 7]
```

#### lists as queues

```
# doing inserts or pops from the beginning of a list is slow, use deque instead
>>> from collections import deque
>>> queue = deque(['Eric','John','Michael'])
>>> queue.append('Terry')
>>> queue
deque(['Eric', 'John', 'Michael', 'Terry'])
>>> queue.popleft()
'Eric'
>>> queue
deque(['John', 'Michael', 'Terry'])
```

#### list comprehensions

```
>>> [x**2 for x in range(10)]
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
>>> [(x,y) for x in [1,2,3] for y in [1,2,3] if x != y]
[(1, 2), (1, 3), (2, 1), (2, 3), (3, 1), (3, 2)]
```

#### comprehensions vs generator

```
# The main advantage of generator over a list is that it take much less memory. 
>>> a = (x**2 for x in range(10))
>>> for x in a:
...   print(x)
```

### tuples

#### constructor

```
() # empty tuple
'a', or ('a',) # singletop tuple
'a','b' or ('a','b') # separate with commas 
tuple() or tuple(iterable) # tuple()
```

#### packing, unpacking

```
t = 1,2,3
a,b,c = t
```

### set, frozenset

```
# unordered collection with no duplicate elements
```

#### constructor

```
t = {'a','b','c'}
t = set('abc')
t = frozenset('abc')  # frozenset is immutable set
t = frozenset({'a','b','c'})
```

#### set operations

```
>>> a = set('abracadabra')
>>> b = set('alacazam')
>>> a          # unique letters in a
{'a', 'r', 'b', 'c', 'd'}
>>> a - b      # letters in a but not in b
{'r', 'd', 'b'}
>>> a | b      # letters in a or b or both
{'a', 'c', 'r', 'd', 'b', 'm', 'z', 'l'}
>>> a & b      # letters in both a and b
{'a', 'c'}
>>> a ^ b      # letters in a or b but not both
{'r', 'd', 'b', 'm', 'z', 'l'}
>>> a < b      # test if a is subset of b
False
>>> a <= b     # a < b and a != b
```

#### set comprehensions

```
>>> a = {x for x in 'abracadabra' if x not in 'abc'}
>>> a
{'d', 'r'}
```

### dict

```
>>> letters = {'a': 'alpha', 'b': 'bravo'}
>>> letters['c'] = 'charlie'
>>> del letters['a']
>>> 'a' in letters
False
>>> list(letters)
['b', 'c']
>>> letters.get('b', 'default')
'bravo'
>>> letters.get('a', 'default')
'default'
```

#### constructor

```
>>> tel = {'jack': 4098, 'sape': 4139}
>>> dict([('sape', 4139), ('guido', 4127), ('jack', 4098)])
{'sape': 4139, 'guido': 4127, 'jack': 4098}
>>> dict(sape=4139, guido=4127, jack=4098)
{'sape': 4139, 'guido': 4127, 'jack': 4098}
```

#### dict comprehensions

```
>>> {x: x**2 for x in (2, 4, 6)}
{2: 4, 4: 16, 6: 36}
```

#### loop

```
>>> knights = {'gallahad': 'the pure', 'robin': 'the brave'}
>>> for k,v in knights.items():
...   print(k,v)
... 
gallahad the pure
robin the brave
```

control flow
------------

```
# break, continue, pass
```

### if

```
if num > 0:
    print("Positive number")
elif num == 0:
    print("Zero")
else:
    print("Negative number")
```

#### one line

```
'Yes' if fruit == 'Apple' else 'No'
print('kid' if age < 13 else 'teenager' if age < 18 else 'adult')
```

### range

```
>>> list(range(5))
[0, 1, 2, 3, 4]
>>> list(range(5,10))
[5, 6, 7, 8, 9]
>>> list(range(5,10,2))
[5, 7, 9]
```

### loops

#### while

```
# fibonacci
>>> a,b = 0,1
>>> while a < 10:
...   print(a)
...   a,b = b,a+b
```

#### while & else

```
>>> letters = list('abc')
>>> while letters:
...   print(letters.pop())
... else:
...   print('empty list')
... 
c
b
a
empty list
```

#### for

```
>>> for n in range(3,0,-1):
...   print(n)
... else:
...   print('Bum!')
... 
3
2
1
Bum!
```

#### enumerate

```
a = [3, 4, 5]
for i, item in enumerate(a):
    print i, item
```

functions
---------

```
>>> def fib(n):
...   a,b = 0,1
...   while a < n:
...     print(a, end=' ')
...     a,b = b,a+b
...   print()
... 
>>> fib(2000)
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597

>>> fib
<function fib at 0x7ff3f37d1e18>
>>> fib(0)
>>> print(fib(0)) # even function without 'return' return a value
None
```

### arguments

#### default argument values

```
def ask_ok(prompt, retries=4, reminder='Please try again!'):
  ...

ask_ok('Do you really want to quit?')
ask_ok('OK to overwrite the file?', 2)
ask_ok('OK to overwrite the file?', 2, 'Come on, only yes or no!')

# default value are evaluated at the point of function definition in defining scope
i = 5
def f(arg=i):
    print(arg)
i = 6
f() # => 5

# default value is evaluated once

def f(a, L=[]):
    L.append(a)
    return L

print(f(1)) # => [1]
print(f(2)) # => [1,2]
```

#### keyword arguments

```
# In a function call, keyword arguments must follow positional arguments.  
def parrot(voltage, state='a stiff', action='voom', type='Norwegian Blue'):

# possible calls:
parrot(1000)                                          # 1 positional argument
parrot(voltage=1000)                                  # 1 keyword argument
parrot(voltage=1000000, action='VOOOOOM')             # 2 keyword arguments
parrot(action='VOOOOOM', voltage=1000000)             # 2 keyword arguments
parrot('a million', 'bereft of life', 'jump')         # 3 positional arguments
parrot('a thousand', state='pushing up the daisies')  # 1 positional, 1 keyword

# invalid calls
parrot()                     # required argument missing
parrot(voltage=5.0, 'dead')  # non-keyword argument after a keyword argument
parrot(110, voltage=220)     # duplicate value for the same argument
parrot(actor='John Cleese')  # unknown keyword argument
```

#### arbitrary argument list

```
>>> def varpafu(*x): print(x) # arguments collected in a tuple
... 
>>> varpafu()
()
>>> varpafu(34,"Do you like Python?", "Of course")
(34, 'Do you like Python?', 'Of course')

# positional parameters always have to precede the arbitrary parameters
>>> def arithmetic_mean(x, *l):
...   sum = x
...   for i in l:
...     sum += i
...   return sum / (1.0 + len(l))
... 
>>> arithmetic_mean(4,7,9)
6.666666666666667
```

#### combined tuple & dictionary

```
# **name must follow *name
def cheeseshop(kind, *arguments, **keywords):
cheeseshop("Limburger", 
           "It's very runny, sir.",
           "It's really very, VERY runny, sir.",
           shopkeeper="Michael Palin",
           client="John Cleese",
           sketch="Cheese Shop Sketch")
```

#### unpacking argument list

```
# *
>>> def f(x,y,z):
...   print(x,y,z)
... 
>>> p = 1,2,3
>>> f(*p)
1 2 3

# **
>>> def f(a,b,x,y):
...   print(a,b,x,y)
...
>>> d = {'a':'append', 'b':'block','x':'extract','y':'yes'}
>>> f(**d)
('append', 'block', 'extract', 'yes')

# * combined with **
>>> t = (47,11)
>>> d = {'x':'extract','y':'yes'}
>>> f(*t, **d)
(47, 11, 'extract', 'yes')
```

### lambda

```
>>> double = lambda x: x * 2
>>> double(5)
10

>>> list(map(lambda x: x + 42, range(10)))
[42, 43, 44, 45, 46, 47, 48, 49, 50, 51]
```

### documentation

#### documentation string

```
>>> def my_function():
...   """Do nothing, but document it.
...   
...   No, really, it doesn't do anything.
...   """
...   pass
... 
>>> print(my_function.__doc__)
Do nothing, but document it.

  No, really, it doesn't do anything.
```

#### function annotations

```
>>> def f(ham: str, eggs: str = 'eggs') -> str:
...     print("Annotations:", f.__annotations__)
...     print("Arguments:", ham, eggs)
...     return ham + ' and ' + eggs
...
>>> f('spam')
Annotations: {'ham': <class 'str'>, 'return': <class 'str'>, 'eggs': <class 'str'>}
Arguments: spam eggs
'spam and eggs'
```

### decorators

```
def foo():
    # do something

def decorator(func):
    # manipulate func
    return func

foo = decorator(foo)  # Manually decorate

@decorator
def bar():
    # Do something
# bar() is decorated
```

modules
-------

```
# fibo.py
# def fib(n)
#   ...
# def fib2(n)
#   ...

import fibo
fibo.fib(n)

from fibo import fib, fib2 # imports names from a module directly into the importing module’s symbol table
fib(n)

from fibo import * # imports all names except those beginning with an underscore (_)
fib(n)

import fibo as fib # the name following as is bound directly to the imported module
fib.fib(500)

from fibo import fib as fibonacci
fibonacci(500)
```

### \_\_main\_\_

```
# fibo.py
# ...
# if __name__ == "__main__":
#     import sys
#     fib(int(sys.argv[1]))

$ python fibo.py 50
0 1 1 2 3 5 8 13 21 34
```

output formatting
-----------------

### formatted string literals

```
# basic
>>> f'The value of pi is approximately {math.pi}.'
'The value of pi is approximately 3.141592653589793.'
# optional format specifier
>>> f'The value of pi is approximately {math.pi:.3f}.'
'The value of pi is approximately 3.142.'
```

#### columns aligned

```
>>> for name, phone in table.items():
...     print(f'{name:10} ==> {phone:10d}')
...
Sjoerd     ==>       4127
Jack       ==>       4098
Dcab       ==>       7678

more in https://docs.python.org/3/library/string.html#formatspec
```

### str.format()

```
# basic
>>> print('We are the {} who say "{}!"'.format('knights', 'Ni'))
We are the knights who say "Ni!"

# positional arguments
>>> print('{1} and {0}'.format('spam', 'eggs'))
eggs and spam

# keyword arguments
>>> print('This {food} is {adjective}.'.format(food='spam', adjective='absolutely horrible'))
This spam is absolutely horrible.

# positional & keyword arguments combined
>>> print('The story of {0}, {1}, and {other}.'.format('Bill', 'Manfred', other='Georg'))
The story of Bill, Manfred, and Georg.

# pass dict or dict with **
>>> table = {'Sjoerd': 4127, 'Jack': 4098, 'Dcab': 8637678}
>>> print('Jack: {0[Jack]:d}; Sjoerd: {0[Sjoerd]:d}; Dcab: {0[Dcab]:d}'.format(table))
Jack: 4098; Sjoerd: 4127; Dcab: 8637678
>>> print('Jack: {Jack:d}; Sjoerd: {Sjoerd:d}; Dcab: {Dcab:d}'.format(**table))
Jack: 4098; Sjoerd: 4127; Dcab: 8637678
```

reading and writing files
-------------------------

### open

```
f = open('workfile', 'w')
f.close()

# RECOMMENDED: using with makes file is properly closed
>>> with open('workfile') as f:
...     read_data = f.read()
>>> f.closed
True
```

#### modes

```
# 'r'	open for reading (default)
# 'w'	open for writing, truncating the file first
# 'x'	open for exclusive creation, failing if the file already exists
# 'a'	open for writing, appending to the end of the file if it exists
# 'b'	binary mode
# 't'	text mode (default)
# '+'	open a disk file for updating (reading and writing)
# w+b  binary read-write access with truncate
```

### file reading

```
# read entire file
>>> f.read()
'This is the entire file.\n'
# empty string means end of file
>>> f.read()
''

f.read(10) # read takes size as argument
f.readline() # line by line

# "for in" is memory efficient, fast & elegant
>>> for line in f:
...     print(line, end='')

# other ways to read all lines:
list(f)
f.readlines()
```

### file writing

```
>>> f.write('This is a test\n')
15 # returns number of characters written

# always need to convert other types to string
>>> value = ('the answer', 42)
>>> s = str(value)  # convert the tuple to string
>>> f.write(s)
18
```

### json

```
>>> import json
>>> json.dumps([1, 'simple', 'list'])
'[1, "simple", "list"]'

json.dump(x, f) # save to file
x = json.load(f) # read from file
```

### pickle

```
# serialize & deserialize python objects

import pickle

example_dict = {1:"6",2:"2",3:"f"}

# dump
pickle_out = open("dict.pickle","wb")
pickle.dump(example_dict, pickle_out)
pickle_out.close()

# load
pickle_in = open("dict.pickle","rb")
example_dict = pickle.load(pickle_in)
```

Exceptions
----------

### handling

#### try except else

```
for arg in sys.argv[1:]:
    try:
        f = open(arg, 'r')
    except OSError:
        print('cannot open', arg)
    else: # executed if no exception raised from try block 
        print(arg, 'has', len(f.readlines()), 'lines')
        f.close()
```

#### finally (clean-up action)

```
# executed ALWAYS before leaving try 
>>> def divide(x, y):
...     try:
...         result = x / y
...     except ZeroDivisionError:
...         print("division by zero!")
...     else:
...         print("result is", result)
...     finally:
...         print("executing finally clause")
...
>>> divide(2, 1)
result is 2.0
executing finally clause
>>> divide(2, 0)
division by zero!
executing finally clause
>>> divide("2", "1")
executing finally clause
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<stdin>", line 3, in divide
TypeError: unsupported operand type(s) for /: 'str' and 'str'
```

#### unexpected error

```
try:
    f = open('myfile.txt')
    s = f.readline()
    i = int(s.strip())
except OSError as err:
    print("OS error: {0}".format(err))
except ValueError:
    print("Could not convert data to an integer.")
except:
    print("Unexpected error:", sys.exc_info()[0])
    raise
```

#### exception variable

```
>>> try:
...     raise Exception('spam', 'eggs')
... except Exception as inst:
...     print(type(inst))    # the exception instance
...     print(inst.args)     # arguments stored in .args
...     print(inst)          # __str__ allows args to be printed directly,
```

### raising

```
raise NameError('HiThere') # exception instance with argument
raise ValueError # exception class -> implicitly instantiated by calling its constructor with no arguments
```

#### user defined exception

```
class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class InputError(Error):
    """Exception raised for errors in the input.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expression, message):
        self.expression = expression
        self.message = message 
```

classes
-------

```
>>> class Complex:
...     def __init__(self, realpart, imagpart):
...         self.r = realpart
...         self.i = imagpart
...
>>> x = Complex(3.0, -4.5)
>>> x.r, x.i
(3.0, -4.5)
```

### self

```
class Bag:
    def __init__(self):
        self.data = []

    def add(self, x):
        self.data.append(x)

    def addtwice(self, x):
        self.add(x)
        self.add(x)
```

### variables

```
class ClassName(object):
  class_variable = value # value shared across all class instances

  def __init__(instance_variable_value):
    self.instance_variable = instance_variable_value # value specific to instance
```

### inheritance

```
class DerivedClassName(BaseClassName)

class DerivedClassName(Base1, Base2, Base3)
```

#### overriding

```
class Person:
    def __init__(self, first, last, age):
        self.firstname = first
        self.lastname = last
        self.age = age
    def __str__(self):
        return self.firstname + " " + self.lastname + ", " + str(self.age)

class Employee(Person):
    def __init__(self, first, last, age, staffnum):
        super().__init__(first, last, age)
        self.staffnumber = staffnum
    def __str__(self):
        return super().__str__() + ", " +  self.staffnumber
```

with
----

```
with open('file.txt') as f:
    contents = f.read()
```

### using class

```
class CustomOpen(object):
    def __init__(self, filename):
        self.file = open(filename)

    def __enter__(self): # returns context to 'with' block
        return self.file

    def __exit__(self, ctx_type, ctx_value, ctx_traceback): # called when 'with' blovk finishing executing
        self.file.close()

with CustomOpen('file') as f:
    contents = f.read()
```

### using decorator

```
from contextlib import contextmanager

@contextmanager
def custom_open(filename):
    f = open(filename)
    try:
        yield f
    finally:
        f.close()

with custom_open('file') as f:
    contents = f.read()
```
