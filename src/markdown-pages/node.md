---
slug: "node"
title: "node"
---
npm
---

### packages

```
npm install lodash
npm install lodash --save // adds entry to package.json
npm uninstall lodash --save // removes package with entry in package.json
npm ls // lists local packages
npm outdated // shows outdated packages

npm install jshint -g // installs globally
npm ls -g
```
