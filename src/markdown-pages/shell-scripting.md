---
slug: "shell-scripting"
title: "shell scripting"
---
### sed

```
sed 's/:.*//' /etc/passwd | # => remove everything after first :
sort -u                   | # => sort and remove duplicates

find /home/tolstoj -type d -print | 
sed 's:/home/tolstoj/:/home/lt/:' |
sed 's/^/mkdir /'                 |
sh -x
```

#### \\1 anchors

```
echo /home/tolstoj/ | sed 's;\(/home\)/tolstoj/;\1/lt/;' # => /home/lt/
```

#### & use matched string

```
sed 's/Warszawa/&, Paryż północy/' < targi.xml.old > targi.xml // replaces 'Warszawa' with 'Warszawa, Paryż północy'
```

#### g(lobal)

```
echo Tolstoja dobrze sie czyta. Tolstoj niezle pisze. > tolstoj.txt

sed 's/Tolstoj/Camus/' tolstoj.txt # => Camusa dobrze sie czyta. Tosltoj niezle pisze.
sed 's/Tolstoj/Camus/g' tolstoj.txt # => Camusa dobrze sie czyta. Camus niezle pisze.
sed 's/Tolstoj/Camus/2' tolstoj.txt # => Tolstoja dobrze sie czyta. Camus niezle pisze.

sed -e '1,10s/zaczarowanie/zablokowanie/g' mojplik2.txt # first ten lines
sed -e '/oldfunc/s/$/# migracja to newfunc/g' mojplik2.txt # only lines matched to oldfunc
sed -e '/newfunc/!s/$/# migracja to newfunc/g' mojplik2.txt # only lines not matched to newfunc
sed -e '/^$/,/^END/s/wzgórza/góry/g' mojplik3.txt  #only blocks of text which start with empty line and ends with line that starts with END
```

#### \-e multiple

```
sed -e 's/to/tamto/g' -e 's/chabeta/szkapa/g' file_origin.xml > file_result.xml
```

#### \-f file

```
cat koretka.sed
s/to/tamto/g
s/chabeta/szkapa/g
...

sed -f korekta.sed file_origin.xml > file_result.xml

# common address for multiple substitutions
1,20{
        s/[Ll]inux/GNU\/Linux/g
        s/samba/Samba/g
        s/posix/POSIX/g
}
# or 
1,/^END$/{
        s/[Ll]inux/GNU\/Linux/g
        s/samba/Samba/g
        s/posix/POSIX/g
}
```

#### d delete

```
sed -e 'd' /etc/services # => deletes all
sed -e '1d' /etc/services # => first line deleted
sed -e '1,10d' /etc/services # => first ten lines deleted

sed -e '/^#/d' /etc/services # => all lines beginning with '#' deleted
```

#### \-n --quiet

```
sed -n -e '/^#/p' /etc/services # => prints nothing but lines beginning with '#'
sed -n -e '/POCZATEK/,/KONIEC/p' /moj/plik # => starts printing on line with POCZATEK, stops printing on line with 'KONIEC'
```
