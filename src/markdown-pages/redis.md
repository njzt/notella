---
slug: "redis"
title: "redis"
---
keys & values
-------------

### GET SET GETSET

```
> SET key foo
OK
> GETSET key bar
"foo"
> GET key
"bar"
> DEL key
(integer) 1
> EXISTS key
(integer) 0
```

### INCR INCRBY DECR DECRBY

```
> INCR counter
(integer) 1
> INCRBY counter 10
(integer) 11
> DECR counter 
(integer) 10
> DECRBY counter 5
(integer) 5
```

### MSET MGET

```
> MSET a foo b bar c baz
OK
> MGET a b c
1) "foo"
2) "bar"
3) "baz"
```

### SET EX

```
> SET key foo
OK
> TTL key
(integer) -1
> SET key foo EX 10
OK
> TTL key
(integer) 9
# wait 10 seconds
> TTL key
(integer) -2
> GET key
(nil)
```

### SET NX/XX

```
# Only set the key if it does not already exist.
> GET key
(nil)
> SET key foo NX
OK
> SET key foo NX
(nil)
# Only set the key if it already exists.
> GET key
(nil)
> SET key foo XX
(nil)
> SET key foo 
OK
> SET key bar XX
OK
```

lists
-----

### LPUSH RPUSH

```
> LPUSH mylist A
(integer) 1
> RPUSH mylist B C
(integer) 3
> LRANGE mylist 0 -1
1) "A"
2) "B"
3) "C"
> LLEN mylist
3
```

### LTRIM

```
127.0.0.1:6379> RPUSH mylist a b c d e f g h i j
(integer) 10
127.0.0.1:6379> LTRIM mylist 2 4
OK
127.0.0.1:6379> LRANGE mylist 0 -1
1) "c"
2) "d"
3) "e"
```

### RPOPLPUSH

```
> RPUSH alist 1 2 3
(integer) 3
> RPOPLPUSH alist blist
"3"
> LRANGE alist 0 -1
1) "1"
2) "2"
> LRANGE blist 0 -1
1) "3"
```

### BRPOP BLPOP BRPOPLPUSH

```
# pop element or block list waiting for pop for 5 seconds
> LPUSH mylist foo
(integer) 1
> BLPOP mylist 5
1) "mylist"
2) "foo"
> BLPOP mylist 5
(nil)
(5.09s)
```

### LREM

```
> RPUSH mylist a 0 a 1 a 2 a 3 a
(integer) 9
> LREM mylist -2 a
(integer) 2
> LRANGE mylist 0 -1
1) "a"
2) "0"
3) "a"
4) "1"
5) "a"
6) "2"
7) "3"
> LREM mylist 0 a
(integer) 3
> LRANGE mylist 0 -1
1) "0"
2) "1"
3) "2"
4) "3"
```

hashes
------

```
127.0.0.1:6379> HMSET user:1000 name forest surname gump
OK
127.0.0.1:6379> HGET user:1000 name
"forest"
127.0.0.1:6379> HGETALL user:1000
1) "name"
2) "forest"
3) "surname"
4) "gump"

https://redis.io/commands#hash
```

sets
----

```
> SADD foo 1 2 3
(integer) 3
> SADD foo 2 3 4
(integer) 1
> SMEMBERS foo
1) "1"
2) "2"
3) "3"
4) "4"
127.0.0.1:6379> SCARD foo
(integer) 4
```

### SPOP

```
# takes random element from set
> SADD foo a b c d e
(integer) 5
> SPOP foo
"a"
> SPOP foo
"c"
```

### SINTER SINTERSTORE

```
> SADD foo 1 2 3
(integer) 3
> SADD bar 2 3 4
(integer) 3
> SINTER foo bar
1) "2"
2) "3"
> SINTERSTORE foobar foo bar
(integer) 2
> SMEMBERS foobar
1) "2"
2) "3"
```

### SUNION SUNIONSTORE

```
> SADD foo 1 2 3
(integer) 3
> SADD bar 3 4
(integer) 2
> SUNION foo bar
1) "1"
2) "2"
3) "3"
4) "4"
> SUNIONSTORE foobar foo bar
(integer) 4
> SMEMBERS foobar
1) "1"
2) "2"
3) "3"
4) "4"
```

sorted sets
-----------

```
> ZADD planets 2440 Mercury 6052 Venus 6371 Earth 3390 Mars
(integer) 4
> ZADD planets 69911 Jupiter 58232 Saturn 25362 Uran 24622 Neptune
(integer) 4
```

### ZRANGE ZREVRANGE

```
> ZRANGE planets 0 -1
1) "Mercury"
2) "Mars"
3) "Venus"
4) "Earth"
5) "Neptune"
6) "Uran"
7) "Saturn"
8) "Jupiter"
> ZREVRANGE planets 0 2 WITHSCORES
1) "Jupiter"
2) "69911"
3) "Saturn"
4) "58232"
5) "Uran"
6) "25362"
```

### ZRANGEBYSCORE ZREVRANGEBYSCORE

```
> ZREVRANGEBYSCORE planets inf 10000
1) "Jupiter"
2) "Saturn"
3) "Uran"
4) "Neptune"

# excluded max
> ZRANGEBYSCORE planets -inf (6371 WITHSCORES
1) "Mercury"
2) "2440"
3) "Mars"
4) "3390"
5) "Venus"
6) "6052"
```

### ZRANK ZREVRANK

```
> ZRANK planets Mercury
(integer) 0
> ZREVRANK planets Earth
(integer) 4
```

### ZREMRANGEBYSCORE

```
> ZREMRANGEBYSCORE planets -inf 10000 
(integer) 4
> ZRANGE planets 0 -1
1) "Neptune"
2) "Uran"
3) "Saturn"
4) "Jupiter"
```
