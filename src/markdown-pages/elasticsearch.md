---
slug: "elasticsearch"
title: "elasticsearch"
---
indices
-------

```
PUT /customer?pretty # create index
GET /_cat/indices?v # show all
DELETE /customer?pretty # delete
```

documents
---------

### create

#### with id

```
PUT twitter/tweet/1?op_type=create # op_type=create makes the request with fail if tweet/1 already exists
{
    "user" : "kimchy",
    "post_date" : "2009-11-15T14:12:12",
    "message" : "trying out Elasticsearch"
}
```

#### automatic id generation

```
POST twitter/tweet/
{
    "user" : "kimchy",
    "post_date" : "2009-11-15T14:12:12",
    "message" : "trying out Elasticsearch"
}
```

### get

```
GET twitter/tweet/0

{
    "_index" : "twitter",
    "_type" : "tweet",
    "_id" : "0",
    "_version" : 1,
    "found": true,
    "_source" : {
        "user" : "kimchy",
        "date" : "2009-11-15T14:12:12",
        "likes": 0,
        "message" : "trying out Elasticsearch"
    }
}

GET twitter/tweet/1/_source # only _source

GET twitter/tweet/0?_source=false # without _source
GET twitter/tweet/0?_source_include=*.id&_source_exclude=entities # filter _source fields
GET twitter/tweet/0?_source=*.id,retweeted # only _source_include
```

#### check existence

```
HEAD twitter/tweet/0
```

### delete

```
DELETE /twitter/tweet/1
```

#### delete by query

```
POST twitter/_delete_by_query
{
  "query": { # use Search API
    "match": {
      "message": "some message"
    }
  }
}
```

### update

#### script

```
POST test/type1/1/_update # increment the counter
{
    "script" : {
        "inline": "ctx._source.counter += params.count",
        "lang": "painless",
        "params" : {
            "count" : 4
        }
    }
}
POST test/type1/1/_update # adds new field
{
    "script" : "ctx._source.new_field = 'value_of_new_field'"
}
POST test/type1/1/_update # deletes if tags contains "green"
{
    "script" : {
        "inline": "if (ctx._source.tags.contains(params.tag)) { ctx.op = 'delete' } else { ctx.op = 'none' }",
        "lang": "painless",
        "params" : {
            "tag" : "green"
        }
    }
}
```

#### doc

```
POST test/type1/1/_update # simple recursive merge, "doc" ignored if "scripts" specified
{
    "doc" : {
        "name" : "new_name"
    }
}
```

### upsert

```
POST test/type1/1/_update
{
    "script" : { # if document exists then run "script"
        "inline": "ctx._source.counter += params.count",
        "lang": "painless",
        "params" : {
            "count" : 4
        }
    },
    "upsert" : { # if document does not exist then insert new with "upsert"
        "counter" : 1
    }
}
```

#### scripted\_upsert

```
POST sessions/session/dh3sgudg8gsrgl/_update
{
    "scripted_upsert":true, # run "script" no matter document exists or not
    "script" : {
        "id": "my_web_session_summariser",
        "params" : {
            "pageViewEvent" : {
                "url":"foo.com/bar",
                "response":404,
                "time":"2014-01-01 12:32"
            }
        }
    },
    "upsert" : {}
}
```

#### doc\_as\_upsert

```
POST test/type1/1/_update
{
    "doc" : {
        "name" : "new_name"
    },
    "doc_as_upsert" : true # use "doc" no matter if document exists or not
}
```
