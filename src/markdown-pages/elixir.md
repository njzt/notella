---
slug: "elixir"
title: "elixir"
---
basic types
-----------

### booleans and atoms

```
# everything is truthy except for false and nil
iex(62)> true 
true
iex(63)> false
false
iex(64)> :true
true
iex(65)> :foo
:foo
iex(68)> :true === true
true
iex(69)> is_boolean(:true)
true
iex(70)> is_atom(true)
true
iex(71)> :nil === nil
true
```

### numbers

#### integers

```
# https://hexdocs.pm/elixir/Integer.html#summary
iex(21)> 255
255
iex(23)> 0b1101
13
iex(24)> 0o644
420
iex(25)> 0x1F
31
```

#### floats

```
# https://hexdocs.pm/elixir/Float.html#summary
iex(26)> 3.14
3.14
iex(27)> 3.0 == 3
true
iex(28)> 3.0===3
false
```

#### operations

```
iex(13)> 2
2
iex(14)> 2+2
4
iex(15)> 2-1
1
iex(16)> 2*5
10
iex(17)> 10/5
2.0
iex(20)> div(10,3)
3
iex(19)> rem(10,3)
1
```

#### methods

```
iex(36)> round(1.2)                           
1
iex(37)> Float.round(1.222,1)                 
1.2
iex(38)> trunc(1.2)                           
1
iex(39)> Integer.digits(12377)                
[1, 2, 3, 7, 7]
iex(40)> Integer.to_string(12377)             
"12377"
iex(41)> Float.parse("123.22 million dollars")
{123.22, " million dollars"}
```

### strings & charlists

```
# Each value in a charlist is the Unicode code point of a character whereas in a binary, the codepoints are encoded as UTF-8.
iex> 'hełło'
[104, 101, 322, 322, 111] # 322 unicode codepoint for ł
iex> "hełło" <> <<0>>
<<104, 101, 197, 130, 197, 130, 111, 0>> # ł encoded in UTF8 as two bytes 197, 130

iex(56)> is_binary("hello")
true
iex(57)> is_binary('hello')
false
```

#### strings

```
# sequence of bytes
iex(49)> w = "world"
"world"
iex(50)> "hello #{w}" <> "!"
"hello world!"
iex(51)> "hello" <> <<0>>   
<<104, 101, 108, 108, 111, 0>> # with byte 0 not a valid string anymore so displayed as binary
```

#### charlists

```
# character lists
iex(54)> ?ł # get character codepoint
322
```

#### string methods

```
# https://hexdocs.pm/elixir/String.html#summary
assert "hello world" == "hello " <> "world"                                                                                                                                     
assert ["hello", "world"] == String.split("hello world", " ")                                                                                                                   
assert String.replace("An awful day", "awful", "incredible") == "An incredible day"                                                                                             
assert true == String.contains?("An incredible day", "day")                                                                                                                     
assert "banana" == String.reverse("ananab")                                                                                                                                     
assert String.trim("  \n banana\n  ") == "banana"                                                                                                                               
assert String.duplicate("String", 3) == "StringStringString"                                                                                                                    
assert String.upcase("listen") == "LISTEN"
```

### tuples

```
# Tuples are intended as fixed-size containers for multiple elements. To manipulate a collection of elements, use a list instead.
iex(16)> tuple = {3.14, :pie, "Apple"} 
{3.14, :pie, "Apple"}
iex(17)> Tuple.append(tuple, :foo)     
{3.14, :pie, "Apple", :foo}
iex(18)> tuple
{3.14, :pie, "Apple"}
iex(19)> Tuple.delete_at(tuple, 0)
{:pie, "Apple"}
iex(20)> Tuple.insert_at(tuple, 1, :foo)
{3.14, :foo, :pie, "Apple"}
```

collections
-----------

### lists

```
# Elixir implements list collections as linked lists. Accessing the list length is an operation that will run in linear time (O(n)). 
iex(1)> list = [3.14, :pie, "Apple"]
[3.14, :pie, "Apple"]
iex(2)> ["appending is fast" | list]
["appending is fast", 3.14, :pie, "Apple"]
iex(3)> [list] ++ ['prepending', :is, :slow] # list concatenation
[[3.14, :pie, "Apple"], 'prepending', :is, :slow]
iex(4)> list ++ ['prepending', :is, :slow]  
[3.14, :pie, "Apple", 'prepending', :is, :slow]
iex(5)> [:foo, 1, :foo, 2, :foo, 3] -- [:foo, 3] # list substraction
[1, :foo, 2, :foo]
iex(7)> [1,2,3] -- [2.0,3] # substraction uses strict comparision
[1, 2]
```

#### head & tail

```
iex(8)> hd list
3.14
iex(9)> tl list
[:pie, "Apple"]
iex(10)> [head | tail] = list # | pattern matching and cons operator "|"
[3.14, :pie, "Apple"]
iex(11)> head
3.14
iex(12)> tail
[:pie, "Apple"]
```

#### methods

```
# https://hexdocs.pm/elixir/List.html#summary
assert List.delete([:a, :b, :c], :b) == [:a,:c]                                                                                                                                 
assert List.delete_at([:a, :b, :c], 2) == [:a,:b]                                                                                                                               
assert List.duplicate("life", 3) == ["life","life","life"]                                                                                                                      
assert List.flatten([1, [2, 3], 4, [5]]) == [1,2,3,4,5]                                                                                                                         
assert List.insert_at([1, 2, 3], 1, 4) == [1,4,2,3]                                                                                                                             
assert List.replace_at([1, 2, 3], 0, 10) == [10,2,3]                                                                                                                            
assert List.replace_at([1, 2, 3], 10, 0) == [1,2,3]                                                                                                                             
assert List.insert_at([1, 2, 3], 10, 4) == [1,2,3,4]                                                                                                                            
assert List.insert_at([1, 2, 3], -1, 4) == [1,2,3,4]                                                                                                                            
assert List.to_tuple([1, 2, 3]) == {1,2,3}                                                                                                                                      
assert List.wrap("value") == ["value"]                                                                                                                                          
assert List.wrap(nil) == []                                                                                                                                                     
assert List.wrap(["value"]) == ["value"]
```

### keyword maps

```
# a list of two-element tuples where the first element of the tuple is an atom and the second element can be any value
# keys are ordered
# keys do not have to be unique
iex(21)> [foo: 'bar', hello: 'world']
[foo: 'bar', hello: 'world']
iex(22)> [{:foo, 'bar'}, {:hello, 'world'}]
[foo: 'bar', hello: 'world']
```

### maps

```
# Unlike keyword lists, they allow keys of any type and are un-ordered
iex(31)> map = %{:foo => 'bar', "hello" => :world}
%{:foo => 'bar', "hello" => :world}
iex(32)> map[:foo]                                
'bar'
iex(33)> map["hello"]                             
:world
```

#### methods

```
https://hexdocs.pm/elixir/Map.html#summary
iex(35)> person = %{first_name: "Jon", last_name: "Snow"}
%{first_name: "Jon", last_name: "Snow"}
iex(36)> person = %{name: "John", age: 25}
%{age: 25, name: "John"}
iex(37)> older_person = Map.put(person, :age, 35)
%{age: 35, name: "John"}
iex(38)> Map.fetch(older_person, :age)
{:ok, 35}
iex(39)> Map.fetch(older_person, :surname)
:error
iex(40)> Map.merge(person, older_person)
%{age: 35, name: "John"}
iex(41)> person_with_hobby = Map.merge(person, %{hobby: "Kayaking"})
%{age: 25, hobby: "Kayaking", name: "John"}
iex(42)> Map.take(person_with_hobby, [:age, :hobby])
%{age: 25, hobby: "Kayaking"}
```

Enum
----

```
# https://hexdocs.pm/elixir/Enum.html
iex> Enum.all?(["foo", "bar", "hello"], fn(s) -> String.length(s) == 3 end)
false
iex> Enum.any?(["foo", "bar", "hello"], fn(s) -> String.length(s) == 5 end)
true
iex> Enum.chunk_every([1, 2, 3, 4, 5, 6], 2)
[[1, 2], [3, 4], [5, 6]]
iex> Enum.chunk_by(["one", "two", "three", "four", "five", "six"], fn(x) -> String.length(x) end)
[["one", "two"], ["three"], ["four", "five"], ["six"]]
iex> Enum.map_every([1, 2, 3, 4, 5, 6, 7, 8], 3, fn x -> x + 1000 end)
[1001, 2, 3, 1004, 5, 6, 1007, 8]
iex> Enum.each(["one", "two", "three"], fn(s) -> IO.puts(s) end)
one
two
three
:ok
iex> Enum.map([0, 1, 2, 3], fn(x) -> x - 1 end)
[-1, 0, 1, 2]
iex> Enum.min([5, 3, 0, -1])
-1
iex> Enum.filter([1, 2, 3, 4], fn(x) -> rem(x, 2) == 0 end)
[2, 4]
iex> Enum.reduce([1, 2, 3], 10, fn(x, acc) -> x + acc end)
16
iex> Enum.sort([:foo, "bar", Enum, -1, 4]) # sort/1
[-1, 4, Enum, :foo, "bar"]
iex> Enum.sort([%{:val => 4}, %{:val => 1}], fn(x, y) -> x[:val] > y[:val] end) # sort/2
[%{val: 4}, %{val: 1}]
iex> Enum.uniq([1, 2, 3, 2, 1, 1, 1, 1, 1])
[1, 2, 3]
iex> Enum.uniq_by([%{x: 1, y: 1}, %{x: 2, y: 1}, %{x: 3, y: 3}], fn coord -> coord.y end)
[%{x: 1, y: 1}, %{x: 3, y: 3}]
```

pattern matching
----------------

```
iex(54)> x = 1
1
iex(55)> [x,y,z] = [1,2,3]
[1, 2, 3]
iex(56)> x
1
iex(57)> [head | tail] = [1,2,3,4]
[1, 2, 3, 4]
iex(58)> head
1
iex(59)> tail
[2, 3, 4]
```

#### examples

```
# tuples
{case File.open("hello.txt", [:write]) do
  {:ok, file} ->
    IO.write(file,"hello world")
    File.close(file)

  {:error, error} ->  
    IO.puts("Error openings the file: #{inspect error}")
end
```

### underscore

```
# For times when you want to pattern match but don't care about capturing any values, you can use the _ underscore variable
iex(8)> [head|_] = [1,2,3,4,5]
[1, 2, 3, 4, 5]
iex(9)> head
1

iex(10)> {_, message} = {:ok, "success"}
{:ok, "success"}
iex(11)> message
"success"
iex(12)> {_, message} = {:error, "failure"}
{:error, "failure"}
iex(13)> message
"failure"
```

### pin operator

```
# when pin a variable we match on the existing value instead of rebinding to a new one
iex(5)> x = 1
1
iex(6)> ^x = 2
** (MatchError) no match of right hand side value: 2
...
iex(6)> {x, ^x} = {2, 1}
{2, 1}
iex(7)> x
2
```

#### examples

```
iex(1)> greeting = "Hello"
"Hello"
iex(2)> greet = fn
...(2)>   (^greeting,name) -> "Hi #{name}"
...(2)>   (greeting,name)  -> "#{greeting} #{name}"
...(2)> end
#Function<13.126501267/2 in :erl_eval.expr/5>
iex(3)> greet.("Hello", "Fred")
"Hi Fred"
iex(4)> greet.("Hi", "Barney") 
"Hi Barney"
```

control structures
------------------

### case

```
case {:ok, "Hello world"} do
...(16)>   {:ok, result} -> "Got result: #{result}" 
...(16)>   {:error} -> "Uh, oh!"
...(16)>   _ -> "Catch all"
...(16)> end
"Got result: Hello world"
```

#### case & pin

```
iex(17)> pie = 3.14
3.14
iex(18)> case "Cherry pie" do
...(18)>   ^pie -> "Not so tasty" # match against existing variable
...(18)>   pie  -> "I bet #{pie} is tasty"
...(18)> end
"I bet Cherry pie is tasty"
```

### cond

```
iex(3)> cond do                              
...(3)>   2 + 2 == 5 -> "This is not true"    # if
...(3)>   2 * 2 == 5 -> "Nor this"            # elsif
...(3)>   1 + 1 == 2 -> "This is true"        # elsif
...(3)>   true       -> "This is like 'else'" # else
...(3)> end                                  
"This is true"
```

#### CondClauseError

```
iex(5)> cond do                       
...(5)>   2 + 2 == 5 -> "This is not true"   
...(5)>   2 * 2 == 5 -> "Nor this"           
...(5)> end
** (CondClauseError) no cond clause evaluated to a truthy value
```

### if unless

```
iex(5)> if String.valid?("Hello") do
...(5)>   "Valid string"
...(5)> else
...(5)>   "Invalid string"
...(5)> end
"Valid string"
```

### with

```
iex(6)> user = %{first: "John", last: "Callan"}
%{first: "John", last: "Callan"}
iex(7)> with {:ok, first} <- Map.fetch(user, :first),
...(7)>      {:ok, last}  <- Map.fetch(user, :last),
...(7)>      do: last <> ", " <> first
"Callan, John"
```

#### error

```
%{first: "John"}
iex(11)> with {:ok, first} <- Map.fetch(user, :first),
...(11)>      {:ok, last}  <- Map.fetch(user, :last), 
...(11)>      do: last <> ", " <> first               
:error
```

functions
---------

### anonymous functions

```
iex(13)> sum = fn (a,b) -> a+b end
#Function<13.126501267/2 in :erl_eval.expr/5>
iex(14)> sum.(3,5)                
8
```

#### & shorthand

```
iex(15)> mul = &(&1 * &2)
&:erlang.*/2
iex(16)> mul.(3,5)
15

iex(17)> greet = &"Hello #{&1}"
#Function<7.126501267/1 in :erl_eval.expr/5>
iex(18)> greet.("Stefan")
"Hello Stefan"
```

#### pattern matching

```
iex(1)> handle_result = fn
...(1)>   {:ok, result} -> IO.puts "Handling result ..."
...(1)>   {:error} -> IO.puts "error has occured"
...(1)> end
```

### named functions

```
defmodule Math do
  def sum(a, b) do
    do_sum(a, b)
  end

  defp do_sum(a, b) do # private
    a + b
  end
end

IO.puts Math.sum(1, 2)    #=> 3
IO.puts Math.do_sum(1, 2) #=> ** (UndefinedFunctionError)
```

#### guards

```
defmodule Math do
  def zero?(0) do
    true
  end

  def zero?(x) when is_integer(x) do
    false
  end
end

# or shorter

defmodule Math do
  def zero?(0), do: true
  def zero?(x) when is_integer(x), do: false
end

IO.puts Math.zero?(0)         #=> true
IO.puts Math.zero?(1)         #=> false
```

#### arity

```
defmodule Greeter2 do
  def hello(), do: "Hello, anonymous person!"   # hello/0
  def hello(name), do: "Hello, " <> name  # hello/1
  def hello(name1, name2), do: "Hello, #{name1} and #{name2}"  # hello/2
end
```

### default arguments

```
defmodule Concat do
  def join(a, b, sep \\ " ") do
    a <> sep <> b
  end
end

IO.puts Concat.join("Hello", "world")      #=> Hello world
IO.puts Concat.join("Hello", "world", "_") #=> Hello_world

# one default for multiple clauses

defmodule Concat do
  def join(a, b \\ nil, sep \\ " ")

  def join(a, b, _sep) when is_nil(b) do
    a
  end

  def join(a, b, sep) do
    a <> sep <> b
  end
end 
```

### pipe

```
iex(2)> [1, [2], 3] |> List.flatten() |> Enum.map(fn x -> x * 2 end)
[2, 4, 6]
iex(3)> "Elixir rocks" |> String.upcase() |> String.split()         
["ELIXIR", "ROCKS"]
```

modules
-------

### alias require import use

#### alias

```
defmodule Example do
  alias Sayings.Greetings # same as: alias Sayings.Greetings, as: Greetings
  def greeting(name), do: Greetings.basic(name)
end

defmodule Example do
  alias Sayings.Greetings, as: Hi
  def greeting(name), do: Hi.basic(name)
end

# alias is lexically scoped
defmodule Example do
  def greeting(name) do
    alias Sayings.Greetings, as: Hi
    Hi.basic(name)
  end
end
```

#### import

```
defmodule Math do
  import List, only: [duplicate: 2] # import duplicate with arity 2

  def some_function do
    duplicate(:ok, 10)
  end
end

# lexically scoped
defmodule Math do
  def some_function do
    import List, only: [duplicate: 2]
    duplicate(:ok, 10)
  end
end

# except, only
import List, only: [last: 1]
import List, except: [last: 1]

# all macros, all functions
import Integer, only: :macros
import Integer, only: :functions
```

#### require

```
# require allows to use macros from other module
defmodule Example do
  require SuperMacros

  SuperMacros.do_stuff
end
```

#### use

```
# enables other module to modify current module
defmodule Hello do
  defmacro __using__(_opts) do
    quote do
      def hello(name), do: "Hi, #{name}"
    end
  end
end

defmodule Example do
  use Hello
end

iex> Example.hello("Sean")
"Hi, Sean"
```

### module attributes

#### annotations

```
# reserved attributes: moduledoc, doc, behaviour, before_compile etc.
# markdown
defmodule Math do
  @moduledoc """
  Provides math-related functions.

  ## Examples

      iex> Math.sum(1, 2)
      3

  """

  @doc """
  Calculates the sum of two numbers.
  """
  def sum(a, b), do: a + b
end
```

#### constants

```
defmodule MyServer do
  @my_data 14
  def first_data, do: @my_data # value is read at compilation time and not at runtime
  @my_data 13
  def second_data, do: @my_data
end

defmodule Example do
  @greeting "Hello"

  def greeting(name) do
    ~s(#{@greeting} #{name}.)
  end
end
```

### structs

```
iex> defmodule User do
...>   defstruct [:email, name: "John", age: 27]
...> end
iex> %User{}
%User{age: 27, email: nil, name: "John"}
```

#### enforce\_keys

```
iex> defmodule Car do
...>   @enforce_keys [:make]
...>   defstruct [:model, :make]
...> end
iex> %Car{}
** (ArgumentError) the following keys must also be given when building struct Car: [:make]
```

Comprehensions
--------------

```
iex> for n <- 1..4, do: n * n
[1, 4, 9, 16]
# with pattern matching
iex> values = [good: 1, good: 2, bad: 3, good: 4]
iex> for {:good, n} <- values, do: n * n
[1, 4, 16]
```

#### filter

```
iex> multiple_of_3? = fn(n) -> rem(n, 3) == 0 end
iex> for n <- 0..5, multiple_of_3?.(n), do: n * n
[0, 9]

# or

import Integer
iex> for x <- 1..100,
...>   is_even(x),
...>   rem(x, 3) == 0, do: x
[6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90, 96]
```

#### any enumerable

```
# Keyword Lists
iex> for {_key, val} <- [one: 1, two: 2, three: 3], do: val
[1, 2, 3]

# Maps
iex> for {k, v} <- %{"a" => "A", "b" => "B"}, do: {k, v}
[{"a", "A"}, {"b", "B"}]

# Binaries
iex> for <<c <- "hello">>, do: <<c>>
["h", "e", "l", "l", "o"]
```

#### multiple generators

```
iex> for i <- [:a, :b, :c], j <- [1, 2], do:  {i, j}
[a: 1, a: 2, b: 1, b: 2, c: 1, c: 2]

dirs = ['/home/mikey', '/home/james']
for dir  <- dirs,
    file <- File.ls!(dir),
    path = Path.join(dir, file),
    File.regular?(path) do
  File.stat!(path).size
end
```

#### :into

```
# create map from keyword list
iex> for {k, v} <- [one: 1, two: 2, three: 3], into: %{}, do: {k, v}
%{one: 1, three: 3, two: 2}

# charlist into string
iex> for c <- [72, 101, 108, 108, 111], into: "", do: <<c>>
"Hello"
```

sigils
------

```
iex(1)> ~r/foo|bar/
~r"foo|bar"
iex(2)> ~s(this is a string with "double" quotes, not 'single' ones)
"this is a string with \"double\" quotes, not 'single' ones"
iex(3)> ~s(String with escape codes \x26 #{"inter" <> "polation"})
"String with escape codes & interpolation"
iex(4)> ~S(String without escape codes \x26 without #{interpolation})
"String without escape codes \\x26 without \#{interpolation}"
iex(5)> ~c(this is a char list containing 'single quotes')
'this is a char list containing \'single quotes\''
iex(6)> ~w(foo bar bat)
["foo", "bar", "bat"]
```
