---
slug: "perl-tricks"
title: "Perl tricks"
---
Strings
-------

### translations

```
$string =~ tr/[a,e,i,o,u,y]/[A,E,I,O,U,Y]/; # make all vowels uppercase
```

### splitpath one line regex

```
my($text) = "/etc/sysconfig/network-scripts/ifcfg-eth0";
my($directory, $filename) = $text =~ m/(.*\/)(.*)$/;
```

### count occurences in text

```
my $numtimes = 0;
$numtimes++ while ($text =~ /$word/gx)a #words

$number_of_d = ('doremifasollasido' =~ tr/o/o/) #characters
```

### check if alphabetic (with unicode)

```
if ($var =~ /^\p{Alphabetic}+$/) {   # or just /^\pL+$/
    print "var is purely alphabetic\n";
}
```

Arrays
------

```
$#array /= 2; # cut off second half of array
```

Functions
---------

### wantarray()

```
sub mysub {
  if (wantarray()) {
    print "list context"; # my @a = mysub();
  } elsif (defined wantarray()) {
    print "scalar context"; # my $s = mysub();
  } else {
    print "no context"; # mysub();
  }
}
```

### named parameters with default values

```
iterate(STEP => 10,FINISH => 100);

sub iterate {
  my %args = (
    START => 0,
    STEP  => 1,
    FINISH=> 10,
    @_
  );
  if ($args{START}) ...
}
```

### nested functions

```
sub outer {
  my $x = $_[0] + 35;
  local *inner = sub { return $x * 19 };
  return $x + inner();
}
```

Private
-------

### variables

```
{
  my $variable;
  sub mysub {
  }
}
```

### private methods in packages

```
package Some_Module;

my secret_function = sub {
}

sub regular_function {
  $secret_function->(ARG1, ARG2);
}
```
