import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"

const Preview = ({ note }) => (
    <div>
        <Link to={`/note/${note.frontmatter.slug}`}>
            <h3>{note.frontmatter.title}</h3>
        </Link>
    </div>
)

Preview.propTypes = {
    note: PropTypes.object
}

export default Preview