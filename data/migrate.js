const TurndownService = require("turndown")
const fs = require("fs")
const path = require("path")

const slugify = str =>
  str
    .replace(/^\s+|\s+$/g, "") // trim
    .toLowerCase()
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-") // collapse dashes

const injectFrontmatter = ({ slug, title, content }) => `---
slug: "${slug}"
title: "${title}"
---
${content}
`

const regSection = new RegExp("<section>(.*?)</section>", "gs")
const regArticle = new RegExp("<h1>(.*?)</h1><article>(.*?)</article>", "s")

const htmlFilePath = path.join(__dirname, "devclippings-backup.html")
const mdFilePath = slug =>
  path.join(__dirname, `../src/markdown-pages/${slug}.md`)
fs.readFile(htmlFilePath, { encoding: "utf-8" }, (err, data) => {
  if (err) console.error(err)

  const mdArticles = {}
  while ((result = regSection.exec(data))) {
    // const { title, mdArticle, htmlArticle } = sectionToMarkdown(result[1]);
    const { title, mdArticle } = sectionToMarkdown(result[1])
    mdArticles[title] = mdArticle

    const slug = slugify(title)
    const content = injectFrontmatter({ title, slug, content: mdArticle })
    console.log(slug, title)
    const mdFile = mdFilePath(slug)
    fs.writeFile(mdFilePath(slug), content, err => {
      if (err) return console.error(err)
      console.log(`${title} saved in ${mdFile}`)
    })
  }
})

const turndownService = new TurndownService({ codeBlockStyle: "fenced" })
// turndownService.addRule("codeBlock", {
//   filter: node => node.nodeName === "DIV" && node.className === "text",
//   replacement: (content, _, options) =>
//     options.fence + "\n" + content + "\n" + options.fence,
// })
// const turndownService = new TurndownService()
function sectionToMarkdown(section) {
  if ((result = regArticle.exec(section))) {
    const [_, title, htmlArticle] = result
    const mdArticle = turndownService.turndown(htmlArticle)
    return {
      title,
      mdArticle,
      htmlArticle,
    }
  }
  console.error("sectionToMarkdown cannot process section:", section)
}
