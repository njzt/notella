// shipitfile.js
module.exports = shipit => {
  // Load shipit-deploy tasks
  require("shipit-deploy")(shipit)
  require("shipit-shared")(shipit)

  shipit.initConfig({
    default: {
      deployTo: "/var/www/apps/notella",
      repositoryUrl: "git@bitbucket.org:njzt/notella.git",
      keepReleases: 5,
      shared: {
        overwrite: true,
        dirs: ["node_modules"],
      },
    },
    production: {
      servers: "njzt@dev.magineo.pl",
      branch: "master",
    },
  })

  shipit.on('updated', async () => {
    await shipit.remote(`source ~/.bashrc && cd ${shipit.releasePath} && npm install --production`)
    await shipit.remote(`source ~/.bashrc && cd ${shipit.releasePath} && npm run build`);
  });
}
